FROM openjdk:11-jre-slim
MAINTAINER by.maksimovich
COPY controller/target/controller-0.0.1-SNAPSHOT.jar scooter-rental-1.0.jar
ENTRYPOINT ["java","-jar","/scooter-rental-1.0.jar"]