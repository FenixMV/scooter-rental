package by.maksimovich.rental;


import by.maksimovich.rental.entity.Model;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Maksim Maksimovich
 */
public interface ModelRepository extends JpaRepository<Model, Long> {
    Optional<Model> findByModification(String modification);
}
