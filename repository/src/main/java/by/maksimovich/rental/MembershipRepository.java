package by.maksimovich.rental;

import by.maksimovich.rental.entity.Membership;
import by.maksimovich.rental.entity.MembershipStatusInRentalPoint;
import by.maksimovich.rental.entity.RentalPoint;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Maksim Maksimovich
 */
public interface MembershipRepository extends JpaRepository<Membership, Long> {
    List<Membership> findMembershipByRentalPointAndMembershipStatus(RentalPoint rentalPoint,
                                                                    MembershipStatusInRentalPoint membershipStatus);

    Optional<Membership> findMembershipByRentalPointIdAndId(Long rentalPointId, Long id);

    Optional<Membership> findMembershipByIdAndRentalPointIdAndMembershipStatus(Long membershipId,
                                                                               Long rentalPointId,
                                                                               MembershipStatusInRentalPoint status);
}
