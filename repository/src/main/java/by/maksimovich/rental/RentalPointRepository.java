package by.maksimovich.rental;

import by.maksimovich.rental.entity.RentalPoint;
import by.maksimovich.rental.entity.RentalPointStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
public interface RentalPointRepository extends JpaRepository<RentalPoint, Long> {
    List<RentalPoint> findRentalPointsByRentalPointStatus(RentalPointStatus rentalPointStatus);
}
