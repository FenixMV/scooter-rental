package by.maksimovich.rental;

import by.maksimovich.rental.entity.PaymentType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Maksim Maksimovich
 */
public interface PaymentTypeRepository extends JpaRepository<PaymentType, Long> {
    Optional<PaymentType> findPaymentTypeByType(String paymentTypeName);
}
