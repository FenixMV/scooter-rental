package by.maksimovich.rental;

import by.maksimovich.rental.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Maksim Maksimovich
 */
public interface OrderRepository extends JpaRepository<Order, Long> {
    Optional<Order> findOrderByIdAndUserId(Long id, Long userId);

    List<Order> findOrdersByUserUsername(String username);

    List<Order> findOrdersByScooterId(Long scooterId);
}
