package by.maksimovich.rental;

import by.maksimovich.rental.entity.Scooter;
import by.maksimovich.rental.entity.ScooterOrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Maksim Maksimovich
 */
public interface ScooterRepository extends JpaRepository<Scooter, Long> {
    List<Scooter> findByModelName(String model);

    Optional<Scooter> findByIdAndRentalPointIdAndAvailabilityStatus(Long id,
                                                                    Long rentalPointId,
                                                                    ScooterOrderStatus availabilityStatus);
}
