package exception;

/**
 * @author Maksim Maksimovich
 */
public class EnterErroneousDataException extends RuntimeException {
    public EnterErroneousDataException() {
    }

    public EnterErroneousDataException(String message) {
        super(message);
    }

    public EnterErroneousDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public EnterErroneousDataException(Throwable cause) {
        super(cause);
    }
}
