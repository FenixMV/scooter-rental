package by.maksimovich.rental.jwt;

import org.springframework.security.core.AuthenticationException;

/**
 * @author Maksim Maksimovich
 */
public class JwtAuthenticationException extends AuthenticationException {
    public JwtAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public JwtAuthenticationException(String msg) {
        super(msg);
    }

}
