package by.maksimovich.rental.config;

import by.maksimovich.rental.jwt.JwtConfigurer;
import by.maksimovich.rental.jwt.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * @author Maksim Maksimovich
 */
@Configuration
@Slf4j
@ComponentScan(basePackages = "by.maksimovich.rental")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtTokenProvider jwtTokenProvider;

    private static final String LOGIN_ENDPOINT = "/api/v1/auth/login";
    private static final String REGISTRATION_ENDPOINT = "/api/v1/user/register";
    private static final String AVAILABLE_RENTAL_POINTS_ENDPOINT = "/rental-point/map-available-rental-points";
    private static final String FIND_ALL_SCOOTERS_ENDPOINT = "/scooter/available-scooters";
    private static final String SWAGGER_ENDPOINT = "/swagger-ui/**";
    private static final String DOC_ENDPOINT = "/api-docs/**";

    @Autowired
    public SecurityConfig(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(REGISTRATION_ENDPOINT).permitAll()
                .antMatchers(SWAGGER_ENDPOINT, DOC_ENDPOINT).permitAll()
                .antMatchers(AVAILABLE_RENTAL_POINTS_ENDPOINT).permitAll()
                .antMatchers(FIND_ALL_SCOOTERS_ENDPOINT).permitAll()
                .antMatchers(LOGIN_ENDPOINT).permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }
}
