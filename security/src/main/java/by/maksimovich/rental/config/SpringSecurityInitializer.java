package by.maksimovich.rental.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Maksim Maksimovich
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
