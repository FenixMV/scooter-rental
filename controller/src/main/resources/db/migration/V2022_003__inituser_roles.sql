CREATE TABLE IF NOT EXISTS user_roles
(
    user_id BIGSERIAL NOT NULL,
    role_id BIGSERIAL NOT NULL,

    FOREIGN KEY (user_id) REFERENCES users_p (id),
    FOREIGN KEY (role_id) REFERENCES roles (id),

    UNIQUE (user_id, role_id)
);
