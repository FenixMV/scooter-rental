CREATE TABLE IF NOT EXISTS scooters
(
    id                          BIGSERIAL PRIMARY KEY,
    power_reserve               NUMERIC(10, 2)                   NOT NULL,
    price_per_minute            NUMERIC(10, 2)                   NOT NULL,
    scooter_availability_status VARCHAR(15)                      NOT NULL,
    model_id                    BIGSERIAL REFERENCES models (id) NOT NULL,
    rental_point_id             BIGSERIAL,
    CONSTRAINT fk_sc_rental_point_id
        FOREIGN KEY (rental_point_id)
            REFERENCES rental_points (id),

    CHECK ( (scooters.power_reserve >= 0)
        AND (scooters.price_per_minute >= 0)
        AND (scooters.scooter_availability_status != '') )
);