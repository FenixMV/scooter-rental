CREATE TABLE IF NOT EXISTS rental_point_payment_types
(
    rental_point_id  BIGSERIAL NOT NULL,
    payment_types_id BIGSERIAL NOT NULL,

    FOREIGN KEY (rental_point_id) REFERENCES rental_points (id),
    FOREIGN KEY (payment_types_id) REFERENCES payment_types (id),

    UNIQUE (rental_point_id, payment_types_id)
);