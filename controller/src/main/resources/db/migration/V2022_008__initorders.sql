CREATE TABLE IF NOT EXISTS orders
(
    id                BIGSERIAL PRIMARY KEY,
    total_price       NUMERIC(10, 2),
    order_time        TIMESTAMP                          NOT NULL,
    order_finish_time TIMESTAMP,
    order_status      VARCHAR(15)                        NOT NULL,
    mileage           NUMERIC(10, 2),
    payment_method    VARCHAR(15)                        NOT NULL,
    user_id           BIGSERIAL REFERENCES users_p (id)    NOT NULL,
    scooter_id        BIGSERIAL REFERENCES scooters (id) NOT NULL,
    membership_id     BIGSERIAL,
    CONSTRAINT fk_ord_membership_id
        FOREIGN KEY (membership_id)
            REFERENCES memberships (id),

    CHECK ( (orders.total_price >= 0) )
);