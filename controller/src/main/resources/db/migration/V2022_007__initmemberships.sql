CREATE TABLE IF NOT EXISTS memberships
(
    id                BIGSERIAL PRIMARY KEY,
    name              VARCHAR(50)                             NOT NULL,
    action_time       INTEGER                                 NOT NULL,
    purchase_time     TIMESTAMP,
    activation_time   TIMESTAMP,
    expiration_time   TIMESTAMP,
    membership_status VARCHAR(15)                             NOT NULL,
    rental_point_id   BIGSERIAL REFERENCES rental_points (id) NOT NULL,
    user_id           BIGSERIAL,
    CONSTRAINT fk_mem_user_id
        FOREIGN KEY (user_id)
            REFERENCES users_p (id),

    CHECK ( (memberships.name != '') )
);