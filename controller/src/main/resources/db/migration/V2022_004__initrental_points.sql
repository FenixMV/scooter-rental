CREATE TABLE IF NOT EXISTS rental_points
(
    id                  BIGSERIAL PRIMARY KEY,
    location            VARCHAR(50) NOT NULL,
    name                VARCHAR(50) NOT NULL,
    scooters_capacity   INTEGER     NOT NULL,
    rental_point_status VARCHAR(10) NOT NULL,
    longitude           NUMERIC     NOT NULL,
    latitude            NUMERIC     NOT NULL,

    CHECK (rental_points.name != '' AND rental_points.scooters_capacity >= 0)
);