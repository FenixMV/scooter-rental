INSERT INTO users_p(id, firstname, lastname, username, password, email, status, registration_date, updated,
                    phone_number)
VALUES (1, 'Maksimovich', 'Maksim', 'maksvv',
        '$2a$10$/HGMVpoCm6BFU5GUx.E7ZeEX1BkTcwnurzAV5pVuXiJi4QUmBdwMO',
        'maksvv.m@yandex.by', 'active', '2022-06-19 00:03:04.705898', null, '+375298217329');

INSERT INTO roles
    (id, name)
VALUES (1, 'ROLE_USER');
INSERT INTO roles
    (id, name)
VALUES (2, 'ROLE_MANAGER');
INSERT INTO roles
    (id, name)
VALUES (3, 'ROLE_ADMIN');

INSERT INTO user_roles(user_id, role_id)
VALUES (1, 1);
INSERT INTO user_roles(user_id, role_id)
VALUES (1, 3);