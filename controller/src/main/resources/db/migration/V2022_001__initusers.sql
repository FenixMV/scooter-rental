CREATE TABLE IF NOT EXISTS users_p
(
    id                BIGSERIAL PRIMARY KEY,
    firstname         VARCHAR(50) NOT NULL,
    lastname          VARCHAR(50) NOT NULL,
    username          VARCHAR(50) NOT NULL UNIQUE,
    password          VARCHAR(120) NOT NULL,
    email             VARCHAR(70) NOT NULL UNIQUE,
    status            VARCHAR(15) NOT NULL,
    registration_date TIMESTAMP   NOT NULL,
    updated           TIMESTAMP,
    phone_number      VARCHAR(13) NOT NULL UNIQUE,

    CHECK ( (users_p.email != '')
        AND (users_p.password != '')
        AND (users_p.lastname != '')
        AND (users_p.firstname != '')
        AND (users_p.username != '') )
);