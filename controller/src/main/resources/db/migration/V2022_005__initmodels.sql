CREATE TABLE IF NOT EXISTS models
(
    id           BIGSERIAL PRIMARY KEY,
    name         VARCHAR(30)    NOT NULL,
    modification VARCHAR(30)    NOT NULL UNIQUE,
    max_speed    NUMERIC(10, 2) NOT NULL,
    max_load     NUMERIC(10, 2) NOT NULL,
    weight       NUMERIC(10, 2) NOT NULL,
    model_status VARCHAR(20)    NOT NULL,

    CHECK ( (models.name != '')
        AND (models.max_speed >= 0)
        AND (models.max_load >= 0)
        AND (models.weight >= 20)
        AND models.modification != '')
);