ALTER TABLE memberships
    ALTER COLUMN user_id DROP NOT NULL;
ALTER TABLE orders
    ALTER COLUMN membership_id DROP NOT NULL;