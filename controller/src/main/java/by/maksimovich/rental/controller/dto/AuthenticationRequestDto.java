package by.maksimovich.rental.controller.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author Maksim Maksimovich
 */
@Data
public class AuthenticationRequestDto {

    @NotBlank
    @NotEmpty
    @Size(min = 1)
    private String username;

    @NotBlank
    @NotEmpty
    @Size(min = 1)
    private String password;

}
