package by.maksimovich.rental.controller.convertor;

import by.maksimovich.rental.controller.dto.membership.MembershipResponse;
import by.maksimovich.rental.controller.dto.membership.MembershipSaveRequest;
import by.maksimovich.rental.entity.Membership;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author Maksim Maksimovich
 */
@Component
@Slf4j
public class MembershipConvertor {
    private final ModelMapper modelMapper;

    public MembershipConvertor() {
        modelMapper = new ModelMapper();
        modelMapper.typeMap(Membership.class, MembershipResponse.class)
                .setPostConverter(convertEntityToDtoResponse());
    }

    private Converter<Membership, MembershipResponse> convertEntityToDtoResponse() {
        return mappingContext -> {
            Membership source = mappingContext.getSource();
            MembershipResponse destination = mappingContext.getDestination();
            destination.setId(source.getId());
            destination.setName(source.getName());
            destination.setActionTime(source.getActionTime());
            destination.setPurchaseTime(source.getPurchaseTime());
            destination.setExpirationTime(source.getExpirationTime());
            destination.setMembershipStatus(source.getMembershipStatus());
            return destination;
        };
    }

    public MembershipResponse convertEntityToDtoResponse(Membership membership) {
        return modelMapper.map(membership, MembershipResponse.class);
    }

    public Membership convertSaveDtoToEntity(MembershipSaveRequest membershipSaveRequest) {
        return modelMapper.map(membershipSaveRequest, Membership.class);
    }
}
