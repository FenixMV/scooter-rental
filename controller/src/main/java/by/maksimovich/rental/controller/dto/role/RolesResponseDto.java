package by.maksimovich.rental.controller.dto.role;

import lombok.Data;

/**
 * @author Maksim Maksimovich
 */
@Data
public class RolesResponseDto {
    private String name;
}
