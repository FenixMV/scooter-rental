package by.maksimovich.rental.controller.convertor;

import by.maksimovich.rental.controller.dto.membership.MembershipResponse;
import by.maksimovich.rental.controller.dto.rentalpoint.RentalPointCreateRequest;
import by.maksimovich.rental.controller.dto.rentalpoint.RentalPointResponse;
import by.maksimovich.rental.controller.dto.rentalpoint.RentalPointUpdateRequest;
import by.maksimovich.rental.controller.dto.scooter.ScooterResponse;
import by.maksimovich.rental.entity.Membership;
import by.maksimovich.rental.entity.RentalPoint;
import by.maksimovich.rental.entity.Scooter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@Component
public class RentalPointConvertor {
    private final ModelMapper modelMapper;
    private final ScooterConvertor scooterConvertor;
    private final MembershipConvertor membershipConvertor;

    public RentalPointConvertor(ScooterConvertor scooterConvertor, MembershipConvertor membershipConvertor) {
        modelMapper = new ModelMapper();
        modelMapper.typeMap(RentalPoint.class, RentalPointResponse.class)
                .setPostConverter(postConvertorForRentalPointResponse());
        modelMapper.typeMap(RentalPointCreateRequest.class, RentalPoint.class)
                .setPostConverter(postConvertorForRentalPointSaveRequest());
        modelMapper.typeMap(RentalPointUpdateRequest.class, RentalPoint.class)
                .setPostConverter(postConvertorForRentalPointUpdateRequest());
        this.scooterConvertor = scooterConvertor;
        this.membershipConvertor = membershipConvertor;
    }

    private Converter<RentalPointUpdateRequest, RentalPoint> postConvertorForRentalPointUpdateRequest() {
        return mappingContext -> {
            RentalPointUpdateRequest source = mappingContext.getSource();
            RentalPoint destination = mappingContext.getDestination();
            destination.setId(source.getId());
            destination.setName(source.getName());
            destination.setLocation(source.getLocation());
            destination.setScootersCapacity(source.getScootersCapacity());
            destination.setRentalPointStatus(source.getRentalPointStatus());
            destination.setLatitude(source.getLatitude());
            destination.setLongitude(source.getLongitude());
            return destination;
        };
    }

    private Converter<RentalPointCreateRequest, RentalPoint> postConvertorForRentalPointSaveRequest() {
        return mappingContext -> {
            RentalPointCreateRequest source = mappingContext.getSource();
            RentalPoint destination = mappingContext.getDestination();
            destination.setName(source.getName());
            destination.setLocation(source.getLocation());
            destination.setScootersCapacity(source.getScootersCapacity());
            destination.setRentalPointStatus(source.getRentalPointStatus());
            destination.setLatitude(source.getLatitude());
            destination.setLongitude(source.getLongitude());
            return destination;
        };
    }

    private Converter<RentalPoint, RentalPointResponse> postConvertorForRentalPointResponse() {
        return mappingContext -> {
            RentalPoint source = mappingContext.getSource();
            RentalPointResponse destination = mappingContext.getDestination();
            destination.setId(source.getId());
            destination.setName(source.getName());
            destination.setLocation(source.getLocation());
            destination.setScootersCapacity(source.getScootersCapacity());
            destination.setRentalPointStatus(source.getRentalPointStatus());
            destination.setLatitude(source.getLatitude());
            destination.setLongitude(source.getLongitude());
            Set<Scooter> scooters = source.getScooters();
            Set<ScooterResponse> scooterResponses = scooters.stream()
                    .map(scooterConvertor::convertEntityToDtoResponse)
                    .collect(Collectors.toSet());
            destination.setScooters(scooterResponses);
            List<Membership> memberships = source.getMemberships();
            List<MembershipResponse> membershipResponses = memberships.stream()
                    .map(membershipConvertor::convertEntityToDtoResponse)
                    .collect(Collectors.toList());
            destination.setMemberships(membershipResponses);
            return destination;
        };
    }

    public RentalPoint convertSaveDtoToEntity(RentalPointCreateRequest rentalPointCreateRequest) {
        return modelMapper.map(rentalPointCreateRequest, RentalPoint.class);
    }

    public RentalPoint convertUpdateDtoToEntity(RentalPointUpdateRequest rentalPointUpdateRequest) {
        return modelMapper.map(rentalPointUpdateRequest, RentalPoint.class);
    }

    public RentalPointResponse convertEntityToDtoResponse(RentalPoint rentalPoint) {
        return modelMapper.map(rentalPoint, RentalPointResponse.class);
    }
}
