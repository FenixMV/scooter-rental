package by.maksimovich.rental.controller.dto.order;

import lombok.Data;

/**
 * @author Maksim Maksimovich
 */
@Data
public class OrderMakeRequest {
    private Long rentalPointId;
    private Long scooterId;
    private Long membershipId;
}
