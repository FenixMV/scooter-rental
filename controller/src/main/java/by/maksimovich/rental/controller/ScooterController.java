package by.maksimovich.rental.controller;

import by.maksimovich.rental.controller.convertor.ScooterConvertor;
import by.maksimovich.rental.controller.dto.scooter.ScooterResponse;
import by.maksimovich.rental.controller.dto.scooter.ScooterSaveRequest;
import by.maksimovich.rental.service.ScooterService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@RestController
@Validated
@RequiredArgsConstructor
@SecurityRequirement(name = "scooter-rental-api")
@RequestMapping("/scooter")
@Slf4j
public class ScooterController {
    private final ScooterService scooterService;
    private final ScooterConvertor scooterConvertor;

    @PostMapping("{modelId}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> addScooter(@RequestBody @Valid ScooterSaveRequest scooterSaveRequest,
                                             @PathVariable Long modelId) {
        scooterService.save(scooterConvertor.convertSaveRequestDtoToEntity(scooterSaveRequest), modelId);
        log.info("scooter successfully saved");
        return ResponseEntity.ok("scooter successfully saved");
    }

    @GetMapping("/show-scooters-by-model")
    @Secured({"ROLE_MANAGER", "ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<List<ScooterResponse>> findScootersByModel(@RequestParam(name = "model") String model) {
        return ResponseEntity.ok(scooterService.findByModelName(model)
                .stream()
                .map(scooterConvertor::convertEntityToDtoResponse)
                .collect(Collectors.toList()));
    }

    @GetMapping("/available-scooters")
    public ResponseEntity<List<ScooterResponse>> findAllScooters() {
        return ResponseEntity.ok(scooterService.findAll()
                .stream()
                .map(scooterConvertor::convertEntityToDtoResponse)
                .collect(Collectors.toList()));
    }

    @DeleteMapping("/{scooterId}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> deleteScooter(@PathVariable Long scooterId) {
        scooterService.delete(scooterId);
        log.info("scooter successfully deleted");
        return ResponseEntity.ok("scooter successfully deleted");
    }
}
