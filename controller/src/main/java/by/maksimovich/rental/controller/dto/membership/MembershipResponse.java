package by.maksimovich.rental.controller.dto.membership;

import by.maksimovich.rental.entity.MembershipStatusInRentalPoint;
import lombok.Data;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

/**
 * @author Maksim Maksimovich
 */
@Data
public class MembershipResponse {
    private Long id;
    private String name;
    private int actionTime;
    private LocalDateTime purchaseTime;
    private LocalDateTime activationTime;
    private LocalDateTime expirationTime;

    @ColumnTransformer(read = "UPPER(membership_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private MembershipStatusInRentalPoint membershipStatus;

    private Long rentalPointId;
    private Long userId;
}
