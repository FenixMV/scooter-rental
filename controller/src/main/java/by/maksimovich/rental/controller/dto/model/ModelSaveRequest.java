package by.maksimovich.rental.controller.dto.model;

import by.maksimovich.rental.entity.ModelStatus;
import lombok.Data;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;
import java.math.BigDecimal;

/**
 * @author Maksim Maksimovich
 */
@Data
public class ModelSaveRequest {

    @NotEmpty(message = "Model name cannot be empty")
    @NotBlank
    @Size(min = 2, message = "Model name should have at least 2 characters")
    private String name;

    @NotEmpty(message = "Model modification cannot be empty")
    @NotBlank
    @Size(min = 1, message = "Model modification should have at least 1 characters")
    private String modification;

    @DecimalMin(value = "20")
    @DecimalMax(value = "300")
    private BigDecimal maxLoad;

    @DecimalMin(value = "5")
    @DecimalMax(value = "60")
    private BigDecimal maxSpeed;

    @DecimalMin(value = "10")
    @DecimalMax(value = "80")
    private BigDecimal weight;

    @ColumnTransformer(read = "UPPER(model_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private ModelStatus modelStatus;
}
