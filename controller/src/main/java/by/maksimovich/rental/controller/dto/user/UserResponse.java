package by.maksimovich.rental.controller.dto.user;

import by.maksimovich.rental.controller.dto.membership.MembershipResponse;
import by.maksimovich.rental.controller.dto.role.RolesResponseDto;
import by.maksimovich.rental.entity.UserStatus;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Maksim Maksimovich
 */
@Data
public class UserResponse {
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private LocalDateTime registrationDate;
    private LocalDateTime updated;
    private String phoneNumber;
    private List<MembershipResponse> memberships;
    private List<RolesResponseDto> userRoles;
    private UserStatus userStatus;
}
