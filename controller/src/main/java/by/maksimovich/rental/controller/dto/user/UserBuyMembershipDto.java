package by.maksimovich.rental.controller.dto.user;

import lombok.Data;

/**
 * @author Maksim Maksimovich
 */
@Data
public class UserBuyMembershipDto {
    private Long rentalPointId;
    private Long membershipId;
}
