package by.maksimovich.rental.controller.dto.scooter;

import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * @author Maksim Maksimovich
 */
@Data
public class ScooterSaveRequest {

    @DecimalMin(value = "0.05")
    private BigDecimal pricePerMinute;

    @DecimalMin(value = "0")
    @DecimalMax(value = "55")
    private BigDecimal powerReserve;

    private Long rentalPointId;
    private Long modelId;
}
