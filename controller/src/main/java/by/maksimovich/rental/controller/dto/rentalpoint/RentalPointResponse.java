package by.maksimovich.rental.controller.dto.rentalpoint;

import by.maksimovich.rental.controller.dto.membership.MembershipResponse;
import by.maksimovich.rental.controller.dto.paymenttype.PaymentTypeResponse;
import by.maksimovich.rental.controller.dto.scooter.ScooterResponse;
import by.maksimovich.rental.entity.RentalPointStatus;
import lombok.Data;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;
import java.util.Set;

/**
 * @author Maksim Maksimovich
 */
@Data
public class RentalPointResponse {
    private Long id;
    private String name;
    private String location;
    private int scootersCapacity;

    @ColumnTransformer(read = "UPPER(rental_point_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private RentalPointStatus rentalPointStatus;

    private double latitude;
    private double longitude;
    private Set<ScooterResponse> scooters;
    private Set<PaymentTypeResponse> paymentTypes;
    private List<MembershipResponse> memberships;
}
