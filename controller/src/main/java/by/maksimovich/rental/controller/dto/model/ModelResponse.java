package by.maksimovich.rental.controller.dto.model;

import by.maksimovich.rental.entity.ModelStatus;
import lombok.Data;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;

/**
 * @author Maksim Maksimovich
 */
@Data
public class ModelResponse {
    private Long id;
    private String name;
    private String modification;
    private BigDecimal maxLoad;
    private BigDecimal maxSpeed;
    private BigDecimal weight;

    @ColumnTransformer(read = "UPPER(model_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private ModelStatus modelStatus;
}
