package by.maksimovich.rental.controller;

import by.maksimovich.rental.controller.dto.AuthenticationRequestDto;
import by.maksimovich.rental.entity.User;
import by.maksimovich.rental.entity.UserStatus;
import by.maksimovich.rental.jwt.JwtTokenProvider;
import by.maksimovich.rental.service.UserService;
import exception.EnterErroneousDataException;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Maksim Maksimovich
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@Validated
@SecurityRequirement(name = "scooter-rental-api")
@RequestMapping("/api/v1/auth/")
public class LoginController {
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid AuthenticationRequestDto requestDto) {
        try {
            String username = requestDto.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
            User user = userService.findByUsername(username);
            if (user == null || UserStatus.DELETED.equals(user.getStatus())
                    || UserStatus.NOT_ACTIVE.equals(user.getStatus())) {
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            }
            String token = jwtTokenProvider.createToken(username, user.getRoles());
            Map<Object, Object> response = new HashMap<>();
            response.put("username", username);
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new EnterErroneousDataException("Invalid username or password");
        }
    }
}
