package by.maksimovich.rental.controller.dto.user;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Maksim Maksimovich
 */
@Data
public class UserHistoryDto {
    private String lastName;
    private String firstName;
    private String username;
    private String email;
    private String phoneNumber;
    private int orderMileage;
    private LocalDateTime rentalStartTime;
    private LocalDateTime rentalEndTime;
}
