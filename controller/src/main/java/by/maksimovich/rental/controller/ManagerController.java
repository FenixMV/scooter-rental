package by.maksimovich.rental.controller;

import by.maksimovich.rental.controller.convertor.OrderConvertor;
import by.maksimovich.rental.controller.convertor.UserConvertor;
import by.maksimovich.rental.controller.dto.order.OrderFinishDto;
import by.maksimovich.rental.controller.dto.order.OrderHistoryScootersResponse;
import by.maksimovich.rental.controller.dto.order.OrderMakeRequest;
import by.maksimovich.rental.controller.dto.user.UserBuyMembershipDto;
import by.maksimovich.rental.controller.dto.user.UserResponse;
import by.maksimovich.rental.service.OrderService;
import by.maksimovich.rental.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@Validated
@SecurityRequirement(name = "scooter-rental-api")
@RequestMapping("/manager")
public class ManagerController {
    private final UserService userService;
    private final UserConvertor userConvertor;
    private final OrderService orderService;
    private final OrderConvertor orderConvertor;

    @GetMapping("/find-user-by-username")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<UserResponse> findByUsername(@RequestParam String username) {
        return ResponseEntity.ok(userConvertor
                .convertUserEntityToUserDtoResponse(userService.findByUsername(username)));
    }

    @PostMapping("/buy-membership/{userId}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    ResponseEntity<String> buyMembership(@PathVariable Long userId,
                                         @RequestBody UserBuyMembershipDto buyMembershipDto) {
        userService.buyMembership(userId,
                buyMembershipDto.getRentalPointId(),
                buyMembershipDto.getMembershipId());
        return ResponseEntity.ok("the user has successfully purchased membership");
    }

    @PostMapping("/make-order-to-user")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> makeOrderToUser(@RequestParam String username,
                                                  @RequestBody OrderMakeRequest orderMakeDto) {
        orderService.makeOrder(userService.findByUsername(username).getId(),
                orderMakeDto.getRentalPointId(),
                orderMakeDto.getScooterId(),
                orderMakeDto.getMembershipId());
        return ResponseEntity.ok("order successfully was made");
    }

    @GetMapping("/show-scooter-order-history/{scooterId}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<List<OrderHistoryScootersResponse>> orderScooterHistory(@PathVariable Long scooterId) {
        return ResponseEntity.ok(orderService.showScooterOrderHistory(scooterId)
                .stream()
                .map(orderConvertor::convertEntityToDtoToHistoryScooterResponse)
                .collect(Collectors.toList()));
    }

    @PostMapping("/finish-user-order")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> finishOrder(@RequestParam String username,
                                              @RequestBody OrderFinishDto orderFinishDto) {
        orderService.finishOrder(userService.findByUsername(username).getId(),
                orderFinishDto.getOrderId());
        return ResponseEntity.ok("order successfully finished");
    }

    @PostMapping("/cancel-user-order")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> cancelOrder(@RequestParam String username,
                                              @RequestBody OrderFinishDto orderFinishDto) {
        orderService.cancelOrder(userService.findByUsername(username).getId(),
                orderFinishDto.getOrderId());
        return ResponseEntity.ok("order successfully canceled");
    }

    @GetMapping("/show-user-history-by-username")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<List<OrderHistoryScootersResponse>> showScooterOrderHistoryByUser(@RequestParam String username) {
        return ResponseEntity.ok(orderService
                .showScooterOrderHistoryByUser(username)
                .stream()
                .map(orderConvertor::convertEntityToDtoToHistoryScooterResponse)
                .collect(Collectors.toList()));
    }
}
