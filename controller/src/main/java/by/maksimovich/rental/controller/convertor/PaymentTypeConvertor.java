package by.maksimovich.rental.controller.convertor;

import by.maksimovich.rental.controller.dto.paymenttype.PaymentTypeResponse;
import by.maksimovich.rental.entity.PaymentType;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author Maksim Maksimovich
 */
@Component
public class PaymentTypeConvertor {
    private final ModelMapper modelMapper;

    public PaymentTypeConvertor() {
        modelMapper = new ModelMapper();
        modelMapper.typeMap(PaymentType.class, PaymentTypeResponse.class)
                .setPostConverter(convertPaymentTypeEntityToResponseDto());
    }

    private Converter<PaymentType, PaymentTypeResponse> convertPaymentTypeEntityToResponseDto() {
        return mappingContext -> {
            PaymentType source = mappingContext.getSource();
            PaymentTypeResponse destination = mappingContext.getDestination();
            destination.setType(source.getType());
            return destination;
        };
    }

    public PaymentTypeResponse convertPaymentTypeEntityToResponseDto(PaymentType paymentType) {
        return modelMapper.map(paymentType, PaymentTypeResponse.class);
    }
}
