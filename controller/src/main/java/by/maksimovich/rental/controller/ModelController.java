package by.maksimovich.rental.controller;

import by.maksimovich.rental.controller.convertor.ModelConvertor;
import by.maksimovich.rental.controller.dto.model.ModelResponse;
import by.maksimovich.rental.controller.dto.model.ModelSaveRequest;
import by.maksimovich.rental.controller.dto.model.ModelUpdateRequest;
import by.maksimovich.rental.service.ModelService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@RestController
@Validated
@RequiredArgsConstructor
@SecurityRequirement(name = "scooter-rental-api")
@RequestMapping("/model")
@Slf4j
public class ModelController {
    private final ModelService modelService;
    private final ModelConvertor modelConvertor;

    @PostMapping
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> createNewModel(@RequestBody @Valid ModelSaveRequest modelSaveRequest) {
        modelService.create(modelConvertor.convertDtoSaveRequestToEntity(modelSaveRequest));
        return ResponseEntity.ok("model successfully created");
    }

    @PostMapping("{modelId}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> deleteModel(@PathVariable Long modelId) {
        modelService.delete(modelId);
        return ResponseEntity.ok("model successfully deleted");
    }

    @PutMapping
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> updateModel(@RequestBody @Valid ModelUpdateRequest modelUpdateRequest) {
        modelService.update(modelConvertor.convertUpdateDtoToEntityModel(modelUpdateRequest));
        return ResponseEntity.ok("model successfully updated");
    }

    @GetMapping
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<List<ModelResponse>> findAllModels() {
        return ResponseEntity.ok(modelService.findAll()
                .stream()
                .map(modelConvertor::convertModelToDtoResponse)
                .collect(Collectors.toList()));
    }
}
