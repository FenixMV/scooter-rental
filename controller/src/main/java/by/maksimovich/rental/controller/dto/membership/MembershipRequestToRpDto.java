package by.maksimovich.rental.controller.dto.membership;

import lombok.Data;

/**
 * @author Maksim Maksimovich
 */
@Data
public class MembershipRequestToRpDto {
    private Long rentalPointId;
    private Long membershipId;
}
