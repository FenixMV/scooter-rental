package by.maksimovich.rental.controller.convertor;

import by.maksimovich.rental.controller.dto.user.UserResponse;
import by.maksimovich.rental.controller.dto.user.UserSaveRequest;
import by.maksimovich.rental.controller.dto.user.UserUpdateRequest;
import by.maksimovich.rental.entity.User;
import by.maksimovich.rental.entity.UserStatus;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@Component
public class UserConvertor {
    private final ModelMapper modelMapper;
    private final MembershipConvertor membershipConvertor;
    private final RolesConvertor rolesConvertor;

    public UserConvertor(MembershipConvertor membershipConvertor, RolesConvertor rolesConvertor) {
        modelMapper = new ModelMapper();
        modelMapper.typeMap(UserSaveRequest.class, User.class)
                .setPostConverter(convertUserSaveDtoToEntity());
        modelMapper.typeMap(User.class, UserResponse.class)
                .setPostConverter(convertUserEntityToUserDtoResponse());
        this.membershipConvertor = membershipConvertor;
        this.rolesConvertor = rolesConvertor;
    }

    private Converter<User, UserResponse> convertUserEntityToUserDtoResponse() {
        return mappingContext -> {
            UserResponse destination = mappingContext.getDestination();
            User source = mappingContext.getSource();
            destination.setId(source.getId());
            destination.setFirstName(source.getFirstname());
            destination.setLastName(source.getLastname());
            destination.setEmail(source.getEmail());
            destination.setRegistrationDate(source.getRegistrationDate());
            destination.setPhoneNumber(source.getPhoneNumber());
            destination.setUpdated(source.getUpdated());
            destination.setUserStatus(source.getStatus());
            destination.setMemberships(source.getMemberships()
                    .stream()
                    .map(membershipConvertor::convertEntityToDtoResponse)
                    .collect(Collectors.toList()));
            destination.setUserRoles(source.getRoles()
                    .stream()
                    .map(rolesConvertor::convertEntitySetToDtoSetResponse)
                    .collect(Collectors.toList()));
            return destination;
        };
    }

    private Converter<UserSaveRequest, User> convertUserSaveDtoToEntity() {
        return mappingContext -> {
            User destination = mappingContext.getDestination();
            UserSaveRequest source = mappingContext.getSource();
            destination.setEmail(source.getEmail());
            destination.setFirstname(source.getFirstName());
            destination.setLastname(source.getLastName());
            destination.setPassword(source.getPassword());
            destination.setPhoneNumber(source.getPhoneNumber());
            destination.setUsername(source.getUsername());
            destination.setRegistrationDate(LocalDateTime.now());
            destination.setStatus(UserStatus.ACTIVE);
            return destination;
        };
    }

    public User convertUserSaveDtoToEntity(UserSaveRequest userSaveRequest) {
        return modelMapper.map(userSaveRequest, User.class);
    }

    public User convertUserUpdateDtoToEntity(UserUpdateRequest userUpdateRequest) {
        return modelMapper.map(userUpdateRequest, User.class);
    }

    public UserResponse convertUserEntityToUserDtoResponse(User user) {
        return modelMapper.map(user, UserResponse.class);
    }
}
