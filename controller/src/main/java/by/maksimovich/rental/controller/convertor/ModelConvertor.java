package by.maksimovich.rental.controller.convertor;

import by.maksimovich.rental.controller.dto.model.ModelResponse;
import by.maksimovich.rental.controller.dto.model.ModelSaveRequest;
import by.maksimovich.rental.controller.dto.model.ModelUpdateRequest;
import by.maksimovich.rental.entity.Model;
import by.maksimovich.rental.entity.ModelStatus;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author Maksim Maksimovich
 */
@Component
@Slf4j
public class ModelConvertor {
    private final ModelMapper modelMapper;

    public ModelConvertor() {
        modelMapper = new ModelMapper();
        modelMapper.typeMap(ModelResponse.class, Model.class)
                .setPostConverter(convertModelToDtoResponse());
        modelMapper.typeMap(ModelSaveRequest.class, Model.class)
                .setPostConverter(convertModelToSaveDto());
        modelMapper.typeMap(ModelUpdateRequest.class, Model.class)
                .setPostConverter(convertUpdateDtoToEntityModel());
    }

    private Converter<ModelUpdateRequest, Model> convertUpdateDtoToEntityModel() {
        return mappingContext -> {
            ModelUpdateRequest source = mappingContext.getSource();
            Model destination = mappingContext.getDestination();
            destination.setName(source.getName());
            destination.setModification(source.getModification());
            destination.setWeight(source.getWeight());
            destination.setMaxSpeed(source.getMaxSpeed());
            destination.setMaxLoad(source.getMaxLoad());
            destination.setModelStatus(source.getModelStatus());
            return destination;
        };
    }

    private Converter<ModelSaveRequest, Model> convertModelToSaveDto() {
        return mappingContext -> {
            Model destination = mappingContext.getDestination();
            ModelSaveRequest source = mappingContext.getSource();
            destination.setName(source.getName());
            destination.setWeight(source.getWeight());
            destination.setMaxLoad(source.getMaxLoad());
            destination.setMaxSpeed(source.getMaxSpeed());
            destination.setModification(source.getModification());
            destination.setModelStatus(ModelStatus.PRODUCED);
            return destination;
        };
    }

    private Converter<ModelResponse, Model> convertModelToDtoResponse() {
        return mappingContext -> {
            ModelResponse source = mappingContext.getSource();
            Model destination = mappingContext.getDestination();
            destination.setId(source.getId());
            destination.setName(source.getName());
            destination.setModification(source.getModification());
            destination.setMaxLoad(source.getMaxLoad());
            destination.setWeight(source.getWeight());
            destination.setMaxSpeed(source.getMaxSpeed());
            destination.setModelStatus(source.getModelStatus());
            return destination;
        };
    }

    public ModelResponse convertModelToDtoResponse(Model model) {
        return modelMapper.map(model, ModelResponse.class);
    }

    public Model convertDtoSaveRequestToEntity(ModelSaveRequest modelSaveRequest) {
        return modelMapper.map(modelSaveRequest, Model.class);
    }

    public Model convertUpdateDtoToEntityModel(ModelUpdateRequest modelUpdateRequest) {
        return modelMapper.map(modelUpdateRequest, Model.class);
    }
}
