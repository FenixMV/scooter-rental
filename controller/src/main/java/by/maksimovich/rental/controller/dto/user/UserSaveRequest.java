package by.maksimovich.rental.controller.dto.user;

import lombok.Data;

import javax.validation.constraints.*;

/**
 * @author Maksim Maksimovich
 */
@Data
public class UserSaveRequest {

    @NotEmpty(message = "user firstname cannot be empty")
    @NotBlank
    @Size(min = 2, message = "user firstname should have at least 2 characters")
    private String firstName;

    @NotEmpty(message = "user lastname cannot be empty")
    @NotBlank
    @Size(min = 1, message = "user lastname should have at least 1 characters")
    private String lastName;

    @NotEmpty(message = "user username cannot be empty")
    @NotBlank
    @Size(min = 2, message = "user username should have at least 2 characters")
    private String username;

    @NotEmpty(message = "user email cannot be empty")
    @NotBlank
    @Email
    private String email;

    @NotEmpty(message = "user password cannot be empty")
    @NotBlank
    @Size(min = 8, message = "password should have at least 8 characters")
    private String password;

    @NotEmpty(message = "user phone number cannot be empty")
    @NotBlank
    @Pattern(regexp = "\\+375[0-9]{9}", message = "The phone number should start with +375, then - 9 digits")
    private String phoneNumber;
}
