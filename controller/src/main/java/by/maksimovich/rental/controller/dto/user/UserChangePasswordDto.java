package by.maksimovich.rental.controller.dto.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author Maksim Maksimovich
 */
@Data
public class UserChangePasswordDto {

    @NotEmpty(message = "user password cannot be empty")
    @NotBlank
    @Size(min = 8, message = "password should have at least 8 characters")
    private String password;
}
