package by.maksimovich.rental.controller;

import by.maksimovich.rental.controller.convertor.UserConvertor;
import by.maksimovich.rental.controller.dto.user.*;
import by.maksimovich.rental.service.SecureService;
import by.maksimovich.rental.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author Maksim Maksimovich
 */
@RestController
@Validated
@RequiredArgsConstructor
@SecurityRequirement(name = "scooter-rental-api")
@RequestMapping(value = "/api/v1/user/")
@Slf4j
public class UserController {
    private final UserService userService;
    private final UserConvertor userConvertor;
    private final SecureService secureService;

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody @Valid UserSaveRequest user) {
        userService.register(userConvertor
                .convertUserSaveDtoToEntity(user));
        return ResponseEntity.ok("the user was registered with the name: " + user.getUsername());
    }

    @PutMapping
    @Secured("ROLE_USER")
    public ResponseEntity<String> updateInfoAboutUser(@RequestBody @Valid UserUpdateRequest userUpdateRequest) {
        userService.updateInfo(secureService.getIdCurrentUser(),
                userUpdateRequest.getLastName(),
                userUpdateRequest.getFirstName());
        return ResponseEntity.ok("info successfully updated");
    }

    @PostMapping("/change-password")
    @Secured({"ROLE_USER"})
    public ResponseEntity<String> changePassword(@RequestBody @Valid UserChangePasswordDto userChangePasswordDto) {
        userService.changePassword(secureService.getIdCurrentUser(), userChangePasswordDto.getPassword());
        return ResponseEntity.ok("user password successfully changed");
    }

    @PostMapping("/buy-membership")
    @Secured("ROLE_USER")
    ResponseEntity<String> buyMembership(@RequestBody UserBuyMembershipDto buyMembershipDto) {
        userService.buyMembership(secureService.getIdCurrentUser(),
                buyMembershipDto.getRentalPointId(),
                buyMembershipDto.getMembershipId());
        return ResponseEntity.ok("the user has successfully purchased membership");
    }

    @GetMapping("/show-my-info")
    @Secured("ROLE_USER")
    public ResponseEntity<UserResponse> showMyInfo(){
        return ResponseEntity.ok(userConvertor
                .convertUserEntityToUserDtoResponse(userService
                        .showMyInfo(secureService.getIdCurrentUser())));
    }
}
