package by.maksimovich.rental.controller;

import by.maksimovich.rental.controller.convertor.MembershipConvertor;
import by.maksimovich.rental.controller.convertor.RentalPointConvertor;
import by.maksimovich.rental.controller.dto.membership.MembershipRequestToRpDto;
import by.maksimovich.rental.controller.dto.membership.MembershipResponse;
import by.maksimovich.rental.controller.dto.membership.MembershipSaveRequest;
import by.maksimovich.rental.controller.dto.rentalpoint.RentalPointCreateRequest;
import by.maksimovich.rental.controller.dto.rentalpoint.RentalPointResponse;
import by.maksimovich.rental.controller.dto.rentalpoint.RentalPointUpdateRequest;
import by.maksimovich.rental.controller.dto.scooter.ScooterAddOrDeleteToRpDto;
import by.maksimovich.rental.service.MembershipService;
import by.maksimovich.rental.service.RentalPointService;
import by.maksimovich.rental.service.ScooterService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@RestController
@Validated
@RequiredArgsConstructor
@SecurityRequirement(name = "scooter-rental-api")
@RequestMapping("/rental-point")
@Slf4j
public class RentalPointController {
    private final RentalPointConvertor rpConvertor;
    private final RentalPointService rpService;
    private final ScooterService scooterService;
    private final MembershipConvertor membershipConvertor;
    private final MembershipService membershipService;

    @PostMapping
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> createRentalPoint(@RequestBody @Valid RentalPointCreateRequest rpDtoToSave) {
        log.info("create rental point get started");
        rpService.create(rpConvertor.convertSaveDtoToEntity(rpDtoToSave));
        return ResponseEntity.ok("rental point successfully saved");
    }

    @PutMapping
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> updateRentalPoint(@RequestBody @Valid RentalPointUpdateRequest rpDtoToUpdate) {
        log.info("update info about rental point get started");
        rpService.update(rpConvertor.convertUpdateDtoToEntity(rpDtoToUpdate));
        return ResponseEntity.ok("rental point successfully updated");
    }

    @PostMapping("/delete/{id}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> deleteRentalPoint(@PathVariable Long id) {
        log.info("delete rental point get started");
        rpService.delete(id);
        return ResponseEntity.ok("rental point successfully deleted");
    }

    @GetMapping("/map-available-rental-points")
    public ResponseEntity<List<RentalPointResponse>> showMapWithAvailableRentalPoints() {
        log.info("show map with available rental points");
        return ResponseEntity.ok(rpService.showMapWithAvailableRentalPoints()
                .stream()
                .map(rpConvertor::convertEntityToDtoResponse)
                .collect(Collectors.toList()));
    }

    @GetMapping
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<List<RentalPointResponse>> showMapWithAllRentalPoints() {
        log.info("show map with rental points");
        return ResponseEntity.ok(rpService.showMapWithAllRentalPoints()
                .stream()
                .map(rpConvertor::convertEntityToDtoResponse)
                .collect(Collectors.toList()));
    }

    @PostMapping("/adding-scooter")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> addScooterToRentalPoint(@RequestBody ScooterAddOrDeleteToRpDto scooterAddToRpDto) {
        scooterService.saveScooterInRentalPoint(scooterAddToRpDto.getScooterId(), scooterAddToRpDto.getRentalPointId());
        log.info("scooter successfully added into rental point");
        return ResponseEntity.ok("scooter successfully added");
    }

    @PostMapping("/delete-scooter")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> deleteScooterFromRentalPoint(@RequestBody ScooterAddOrDeleteToRpDto scooterDeleteFromRpDto) {
        scooterService.deleteScooterInRentalPoint(scooterDeleteFromRpDto.getRentalPointId(),
                scooterDeleteFromRpDto.getScooterId());
        log.info("scooter successfully deleted from rental point");
        return ResponseEntity.ok("scooter by: "
                + scooterDeleteFromRpDto.getScooterId() + " id deleted from rental point");
    }

    @GetMapping("/show-memberships/{rpId}")
    @Secured({"ROLE_MANAGER", "ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<List<MembershipResponse>> findAllMembershipsInRentalPoint(@PathVariable Long rpId) {
        return ResponseEntity.ok(membershipService.findAllMembershipsInRentalPoint(rpId)
                .stream()
                .map(membershipConvertor::convertEntityToDtoResponse)
                .collect(Collectors.toList()));
    }

    @PostMapping("/add-membership/{rentalPointId}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> addMembershipInRentalPoint(@RequestBody @Valid MembershipSaveRequest membershipSaveRequest,
                                                             @PathVariable Long rentalPointId) {
        membershipService.addMembershipInRentalPoint(membershipConvertor
                .convertSaveDtoToEntity(membershipSaveRequest), rentalPointId);
        log.info("membership saved");
        return ResponseEntity.ok("Membership successfully added to rental point by: " + rentalPointId + " id");
    }

    @PostMapping("/delete-membership")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> deleteMembershipFromRentalPoint(@RequestBody MembershipRequestToRpDto membershipRequestToRpDto) {
        membershipService.deleteMembershipFromRentalPoint(membershipRequestToRpDto.getRentalPointId(),
                membershipRequestToRpDto.getMembershipId());
        return ResponseEntity.ok("membership by: " + membershipRequestToRpDto.getMembershipId()
                + " successfully deleted from rental point by: " + membershipRequestToRpDto.getRentalPointId());
    }

    @PostMapping("/add-new-payment-type/{rpId}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> addNewPaymentTypeInRentalPoint(@PathVariable Long rpId,
                                                                 @RequestParam String paymentType) {
        rpService.addNewPaymentType(rpId, paymentType);
        return ResponseEntity.ok("New payment type: " + paymentType +
                " successfully added to rental point by: " + rpId + " id");
    }

    @PostMapping("/delete-payment-type/{rpId}")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> removePaymentTypeInRentalPoint(@PathVariable Long rpId,
                                                                 @RequestParam String paymentType) {
        rpService.removePaymentType(rpId, paymentType);
        return ResponseEntity.ok("Payment type: " + paymentType +
                " successfully deleted from rental point by: " + rpId + " id");
    }
}
