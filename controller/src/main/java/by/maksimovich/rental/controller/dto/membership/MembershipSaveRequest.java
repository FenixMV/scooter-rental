package by.maksimovich.rental.controller.dto.membership;

import by.maksimovich.rental.entity.MembershipStatusInRentalPoint;
import lombok.Data;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

/**
 * @author Maksim Maksimovich
 */
@Data
public class MembershipSaveRequest {

    @NotEmpty(message = "membership name cannot be empty")
    @NotBlank
    @Size(min = 2, message = "membership name should have at least 2 characters")
    private String name;

    @Min(1)
    @Max(300)
    private int actionTime;

    @PastOrPresent
    private LocalDateTime purchaseTime;

    @PastOrPresent
    private LocalDateTime expirationTime;

    @ColumnTransformer(read = "UPPER(membership_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private MembershipStatusInRentalPoint membershipStatus;

    private Long rentalPointId;
    private Long userId;
}
