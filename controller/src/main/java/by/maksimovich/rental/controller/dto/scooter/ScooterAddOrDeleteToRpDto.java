package by.maksimovich.rental.controller.dto.scooter;

import lombok.Data;

/**
 * @author Maksim Maksimovich
 */
@Data
public class ScooterAddOrDeleteToRpDto {
    private Long scooterId;
    private Long rentalPointId;
}
