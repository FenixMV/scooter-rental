package by.maksimovich.rental.controller;

import by.maksimovich.rental.controller.dto.user.UserChangePasswordDto;
import by.maksimovich.rental.service.AdminService;
import by.maksimovich.rental.service.RoleService;
import by.maksimovich.rental.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author Maksim Maksimovich
 */
@RestController
@Validated
@RequiredArgsConstructor
@RequestMapping("/api/v1/admin")
@Slf4j
@SecurityRequirement(name = "scooter-rental-api")
public class AdminController {
    private final AdminService adminService;
    private final RoleService roleService;
    private final UserService userService;

    @PostMapping("/issue-manager-rights")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> issuanceManagerRights(@RequestParam String username) {
        adminService.issuanceManagerRights(username);
        return ResponseEntity.ok("rights successfully issue");
    }

    @PostMapping("/remove-manager-rights")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> removeManagerRights(@RequestParam String username) {
        adminService.removeManagerRights(username);
        return ResponseEntity.ok("rights successfully removed");
    }

    @PostMapping("/create-new-role")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> createNewRole(@RequestParam String name) {
        roleService.createNewRole(name);
        return ResponseEntity.ok("new role successfully created");
    }

    @PostMapping("/change-user-password")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> changePassword(@RequestParam String username,
                                                 @RequestBody @Valid UserChangePasswordDto userChangePasswordDto) {
        userService.changePassword(userService.findByUsername(username).getId(),
                userChangePasswordDto.getPassword());
        return ResponseEntity.ok("user password successfully changed");
    }

    @PostMapping("/delete-user/{userId}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> deleteUser(@PathVariable Long userId) {
        userService.delete(userId);
        return ResponseEntity.ok("user successfully deleted");
    }
}
