package by.maksimovich.rental.controller.dto.paymenttype;

import lombok.Data;

/**
 * @author Maksim Maksimovich
 */
@Data
public class PaymentTypeResponse {
    private Long id;
    private String type;
}
