package by.maksimovich.rental.controller.handler.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @author Maksim Maksimovich
 */
@Data
public class ValidDetails {
    private HttpStatus status;
    private String message;
    private long timestamp;
}
