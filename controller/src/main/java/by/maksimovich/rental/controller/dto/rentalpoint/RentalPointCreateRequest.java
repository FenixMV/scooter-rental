package by.maksimovich.rental.controller.dto.rentalpoint;

import by.maksimovich.rental.entity.RentalPointStatus;
import lombok.Data;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;

/**
 * @author Maksim Maksimovich
 */
@Data
public class RentalPointCreateRequest {

    @NotBlank(message = "field name cannot be blank")
    @Size(min = 1)
    @NotEmpty(message = "field name cannot be empty")
    private String name;

    @NotBlank(message = "field location cannot be blank")
    @Size(min = 2)
    @NotEmpty(message = "field location cannot be empty")
    private String location;

    @Min(1)
    @Max(100)
    private int scootersCapacity;

    @ColumnTransformer(read = "UPPER(rental_point_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private RentalPointStatus rentalPointStatus;

    private double latitude;
    private double longitude;
}
