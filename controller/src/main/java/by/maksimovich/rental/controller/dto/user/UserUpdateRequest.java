package by.maksimovich.rental.controller.dto.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author Maksim Maksimovich
 */
@Data
public class UserUpdateRequest {

    @NotEmpty(message = "user firstname cannot be empty")
    @NotBlank
    @Size(min = 2, message = "user firstname should have at least 2 characters")
    private String firstName;

    @NotEmpty(message = "user lastname cannot be empty")
    @NotBlank
    @Size(min = 1, message = "user lastname should have at least 1 characters")
    private String lastName;
}
