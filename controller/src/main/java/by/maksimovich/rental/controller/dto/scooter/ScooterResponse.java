package by.maksimovich.rental.controller.dto.scooter;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Maksim Maksimovich
 */
@Data
public class ScooterResponse {
    private Long id;
    private Long modelId;
    private String modelName;
    private String modelModification;
    private BigDecimal weight;
    private BigDecimal maxLoad;
    private BigDecimal maxSpeed;
    private BigDecimal pricePerMinute;
    private BigDecimal powerReserve;
    private Long rentalPointId;
}
