package by.maksimovich.rental.controller.convertor;

import by.maksimovich.rental.controller.dto.order.OrderHistoryScootersResponse;
import by.maksimovich.rental.entity.Order;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author Maksim Maksimovich
 */
@Component
public class OrderConvertor {
    private final ModelMapper modelMapper;

    private final MembershipConvertor membershipConvertor;

    public OrderConvertor(MembershipConvertor membershipConvertor) {
        modelMapper = new ModelMapper();
        modelMapper.typeMap(Order.class, OrderHistoryScootersResponse.class)
                .setPostConverter(convertEntityToDtoToHistoryScooterResponse());
        this.membershipConvertor = membershipConvertor;
    }

    private Converter<Order, OrderHistoryScootersResponse> convertEntityToDtoToHistoryScooterResponse() {
        return mappingContext -> {
            OrderHistoryScootersResponse destination = mappingContext.getDestination();
            Order source = mappingContext.getSource();
            destination.setId(source.getId());
            destination.setUserId(source.getUser().getId());
            destination.setScooterId(source.getScooter().getId());
            destination.setRentalPointId(source.getScooter().getRentalPoint().getId());
            destination.setOrderTime(source.getOrderTime());
            destination.setOrderFinishTime(source.getOrderFinishTime());
            destination.setMileage(source.getMileage());
            destination.setTotalPrice(source.getTotalPrice());
            destination.setRentalPointName(source.getScooter().getRentalPoint().getName());
            destination.setRentalPointLocation(source.getScooter().getRentalPoint().getLocation());
            destination.setFirstName(source.getUser().getFirstname());
            destination.setLastName(source.getUser().getLastname());
            destination.setUsername(source.getUser().getUsername());
            destination.setPaymentMethod(source.getPaymentMethod());
            destination.setOrderStatus(source.getOrderStatus());
            if (source.getMembership() != null) {
                destination.setMembership(membershipConvertor.convertEntityToDtoResponse(source.getMembership()));
            }
            return destination;
        };
    }

    public OrderHistoryScootersResponse convertEntityToDtoToHistoryScooterResponse(Order order) {
        return modelMapper.map(order, OrderHistoryScootersResponse.class);
    }
}
