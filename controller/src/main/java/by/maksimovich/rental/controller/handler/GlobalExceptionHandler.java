package by.maksimovich.rental.controller.handler;

import by.maksimovich.rental.controller.handler.dto.ErrorDetails;
import by.maksimovich.rental.controller.handler.dto.ValidDetails;
import by.maksimovich.rental.jwt.JwtAuthenticationException;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * @author Maksim Maksimovich
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<ValidDetails> handleNoDataFoundException(NoDataFoundException noDataFoundException) {
        ValidDetails validDetails = new ValidDetails();
        validDetails.setStatus(HttpStatus.NOT_FOUND);
        validDetails.setTimestamp(System.currentTimeMillis());
        validDetails.setMessage(noDataFoundException.getMessage());
        return new ResponseEntity<>(validDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exception(Exception ex) {
        return new ResponseEntity<>(getBody(INTERNAL_SERVER_ERROR, ex, "Something Went Wrong"), new HttpHeaders(), INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<ValidDetails> handleEnterErroneousDataException(EnterErroneousDataException erroneousDataException) {
        ValidDetails validDetails = new ValidDetails();
        validDetails.setMessage(erroneousDataException.getMessage());
        validDetails.setStatus(HttpStatus.BAD_REQUEST);
        validDetails.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<>(validDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ValidDetails> handleJwtAuthenticationException(JwtAuthenticationException authenticationException) {
        ValidDetails validDetails = new ValidDetails();
        validDetails.setMessage(authenticationException.getMessage());
        validDetails.setStatus(HttpStatus.BAD_REQUEST);
        validDetails.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<>(validDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ValidDetails> handleDataIntegrityViolationException(DataIntegrityViolationException exception) {
        ValidDetails validDetails = new ValidDetails();
        validDetails.setMessage(exception.getMessage());
        validDetails.setStatus(HttpStatus.BAD_REQUEST);
        validDetails.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<>(validDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ValidDetails> handleConstraintViolationException(ConstraintViolationException exception) {
        ValidDetails validDetails = new ValidDetails();
        validDetails.setMessage(exception.getMessage());
        validDetails.setStatus(HttpStatus.BAD_REQUEST);
        validDetails.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<>(validDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ValidDetails> handleAccessDeniedException(AccessDeniedException accessDeniedException) {
        ValidDetails validDetails = new ValidDetails();
        validDetails.setMessage("Access denied");
        validDetails.setStatus(HttpStatus.UNAUTHORIZED);
        validDetails.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<>(validDetails, HttpStatus.UNAUTHORIZED);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errorList = ex
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        log.error(ex.getLocalizedMessage());
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.BAD_REQUEST, System.currentTimeMillis(), errorList);
        return handleExceptionInternal(ex, errorDetails, headers, errorDetails.getStatus(), request);
    }


    public Map<String, Object> getBody(HttpStatus status, Exception ex, String message) {

        log.error(message, ex);

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", message);
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("error", status.getReasonPhrase());
        body.put("exception", ex.toString());

        Throwable cause = ex.getCause();
        if (cause != null) {
            body.put("exceptionCause", ex.getCause().toString());
        }
        return body;
    }
}
