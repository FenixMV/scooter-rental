package by.maksimovich.rental.controller.dto.order;

import lombok.Data;

/**
 * @author Maksim Maksimovich
 */
@Data
public class OrderFinishDto {
    private Long orderId;
}
