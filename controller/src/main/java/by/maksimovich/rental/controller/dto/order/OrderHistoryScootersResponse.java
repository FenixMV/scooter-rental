package by.maksimovich.rental.controller.dto.order;

import by.maksimovich.rental.controller.dto.membership.MembershipResponse;
import by.maksimovich.rental.entity.OrderStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Maksim Maksimovich
 */
@Data
public class OrderHistoryScootersResponse {
    private Long id;
    private Long userId;
    private Long rentalPointId;
    private Long scooterId;
    private LocalDateTime orderTime;
    private LocalDateTime orderFinishTime;
    private String rentalPointLocation;
    private String rentalPointName;
    private BigDecimal mileage;
    private BigDecimal totalPrice;
    private String lastName;
    private String firstName;
    private String username;
    private String paymentMethod;
    private MembershipResponse membership;
    private OrderStatus orderStatus;
}
