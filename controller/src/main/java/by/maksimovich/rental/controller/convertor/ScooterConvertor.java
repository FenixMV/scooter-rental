package by.maksimovich.rental.controller.convertor;

import by.maksimovich.rental.controller.dto.scooter.ScooterResponse;
import by.maksimovich.rental.controller.dto.scooter.ScooterSaveRequest;
import by.maksimovich.rental.controller.dto.scooter.ScooterUpdateRequest;
import by.maksimovich.rental.entity.Scooter;
import by.maksimovich.rental.entity.ScooterOrderStatus;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author Maksim Maksimovich
 */
@Component
public class ScooterConvertor {
    private final ModelMapper modelMapper;

    public ScooterConvertor() {
        modelMapper = new ModelMapper();
        modelMapper.typeMap(Scooter.class, ScooterResponse.class)
                .setPostConverter(convertEntitySetToDtoSetResponse());
        modelMapper.typeMap(ScooterSaveRequest.class, Scooter.class)
                .setPostConverter(convertSaveDtoToEntity());
    }

    private Converter<ScooterSaveRequest, Scooter> convertSaveDtoToEntity() {
        return mappingContext -> {
            Scooter destination = mappingContext.getDestination();
            ScooterSaveRequest source = mappingContext.getSource();
            destination.setAvailabilityStatus(ScooterOrderStatus.AVAILABLE);
            destination.setPowerReserve(source.getPowerReserve());
            destination.setPricePerMinute(source.getPricePerMinute());
            return destination;
        };
    }


    private Converter<Scooter, ScooterResponse> convertEntitySetToDtoSetResponse() {
        return mappingContext -> {
            ScooterResponse destination = mappingContext.getDestination();
            Scooter source = mappingContext.getSource();
            destination.setId(source.getId());
            destination.setModelId(source.getModel().getId());
            destination.setMaxLoad(source.getModel().getMaxLoad());
            destination.setWeight(source.getModel().getWeight());
            destination.setMaxSpeed(source.getModel().getMaxSpeed());
            destination.setModelName(source.getModel().getName());
            destination.setModelModification(source.getModel().getModification());
            destination.setPowerReserve(source.getPowerReserve());
            destination.setPricePerMinute(source.getPricePerMinute());
            return destination;
        };
    }

    public Scooter convertSaveRequestDtoToEntity(ScooterSaveRequest scooterSaveRequest) {
        return modelMapper.map(scooterSaveRequest, Scooter.class);
    }

    public ScooterResponse convertEntityToDtoResponse(Scooter scooter) {
        return modelMapper.map(scooter, ScooterResponse.class);
    }

    public Scooter convertUpdateRequestDtoToEntity(ScooterUpdateRequest scooterUpdateRequest) {
        return modelMapper.map(scooterUpdateRequest, Scooter.class);
    }
}
