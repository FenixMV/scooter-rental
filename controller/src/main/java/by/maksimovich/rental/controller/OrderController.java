package by.maksimovich.rental.controller;

import by.maksimovich.rental.controller.convertor.OrderConvertor;
import by.maksimovich.rental.controller.dto.order.OrderFinishDto;
import by.maksimovich.rental.controller.dto.order.OrderHistoryScootersResponse;
import by.maksimovich.rental.controller.dto.order.OrderMakeRequest;
import by.maksimovich.rental.service.OrderService;
import by.maksimovich.rental.service.SecureService;
import by.maksimovich.rental.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@RestController
@Validated
@RequiredArgsConstructor
@SecurityRequirement(name = "scooter-rental-api")
@RequestMapping("/order")
@Slf4j
public class OrderController {
    private final OrderConvertor orderConvertor;
    private final OrderService orderService;
    private final UserService userService;
    private final SecureService secureService;

    @PostMapping("/make-order")
    @Secured({"ROLE_MANAGER", "ROLE_ADMIN"})
    public ResponseEntity<String> makeOrder(@RequestBody OrderMakeRequest orderMakeDto) {
        orderService.makeOrder(secureService.getIdCurrentUser(),
                orderMakeDto.getRentalPointId(),
                orderMakeDto.getScooterId(),
                orderMakeDto.getMembershipId());
        return ResponseEntity.ok("order successfully was made");
    }

    @PostMapping("/finish-order")
    @Secured("ROLE_USER")
    public ResponseEntity<String> finishOrder(@RequestBody OrderFinishDto orderFinishDto) {
        orderService.finishOrder(secureService.getIdCurrentUser(), orderFinishDto.getOrderId());
        return ResponseEntity.ok("order successfully finished");
    }

    @GetMapping("/show-history-by-username")
    @Secured("ROLE_USER")
    public ResponseEntity<List<OrderHistoryScootersResponse>> showScooterOrderHistoryByUser() {
        return ResponseEntity.ok(orderService
                .showScooterOrderHistoryByUser(userService.findById(secureService
                        .getIdCurrentUser()).getUsername())
                .stream()
                .map(orderConvertor::convertEntityToDtoToHistoryScooterResponse)
                .collect(Collectors.toList()));
    }
}
