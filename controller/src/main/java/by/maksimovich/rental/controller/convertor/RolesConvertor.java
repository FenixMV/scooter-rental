package by.maksimovich.rental.controller.convertor;

import by.maksimovich.rental.controller.dto.role.RolesResponseDto;
import by.maksimovich.rental.entity.Role;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author Maksim Maksimovich
 */
@Component
public class RolesConvertor {
    private final ModelMapper modelMapper;

    public RolesConvertor() {
            modelMapper = new ModelMapper();
            modelMapper.typeMap(Role.class, RolesResponseDto.class)
                    .setPostConverter(convertEntitySetToDtoSetResponse());
    }

    private Converter<Role, RolesResponseDto> convertEntitySetToDtoSetResponse() {
        return mappingContext -> {
            RolesResponseDto destination = mappingContext.getDestination();
            Role source = mappingContext.getSource();
            destination.setName(source.getName());
            return destination;
        };
    }

    public RolesResponseDto convertEntitySetToDtoSetResponse(Role role) {
        return modelMapper.map(role, RolesResponseDto.class);
    }
}
