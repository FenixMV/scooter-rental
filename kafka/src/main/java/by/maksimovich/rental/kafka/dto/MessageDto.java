package by.maksimovich.rental.kafka.dto;

import lombok.Data;

/**
 * @author Maksim Maksimovich
 */
@Data
public class MessageDto {
    private String text;
    private String subject;
    private String sendTo;
}
