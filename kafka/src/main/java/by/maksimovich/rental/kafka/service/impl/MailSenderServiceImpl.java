package by.maksimovich.rental.kafka.service.impl;

import by.maksimovich.rental.kafka.dto.MessageDto;
import by.maksimovich.rental.kafka.service.MailSenderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Maksim Maksimovich
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class MailSenderServiceImpl implements MailSenderService {
    private final KafkaTemplate<String, MessageDto> kafkaTemplate;

    @Override
    public void sendNotificationAboutUserRegistration(String username, String email) {
        MessageDto message = new MessageDto();
        message.setSubject("Registration");
        message.setText("Good time of day: " + username +
                "\n you have successfully registered in the electric scooter rental system");
        message.setSendTo(email);
        kafkaTemplate.send("messages", message);
    }
}
