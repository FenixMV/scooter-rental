package by.maksimovich.rental.kafka.service;

/**
 * @author Maksim Maksimovich
 */
public interface MailSenderService {
    void sendNotificationAboutUserRegistration(String username, String email);
}
