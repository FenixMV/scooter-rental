package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.MembershipRepository;
import by.maksimovich.rental.entity.Membership;
import by.maksimovich.rental.entity.MembershipStatusInRentalPoint;
import by.maksimovich.rental.entity.RentalPoint;
import by.maksimovich.rental.service.MembershipService;
import by.maksimovich.rental.service.RentalPointService;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MembershipServiceImpl implements MembershipService {
    private final MembershipRepository membershipRepository;
    private final RentalPointService rpService;

    @Override
    @Transactional
    public void update(Membership membership) {
        log.info("membership successfully updated by: " + membership.getId() + " id");
        membershipRepository.save(membership);
    }

    @Override
    @Transactional(readOnly = true)
    public Membership findById(Long membershipId) {
        log.info("membership successfully find by: " + membershipId + " id");
        return membershipRepository.findById(membershipId)
                .orElseThrow(() -> new NoDataFoundException("membership not found by: " + membershipId + " id"));
    }

    @Override
    @Transactional
    public void addMembershipInRentalPoint(Membership membership, Long rentalPointId) {
        RentalPoint rentalPoint = rpService.findById(rentalPointId);
        if (rentalPoint.getPaymentTypes()
                .stream()
                .anyMatch(paymentType -> ("membership").equals(paymentType.getType()))) {
            membership.setRentalPoint(rentalPoint);
            membershipRepository.save(membership);
            log.info("membership by: " + membership.getId() +
                    " id successfully added in rental point by: " + rentalPointId + " id");
        } else {
            log.info("something went wrong with adding membership by: " + membership.getId() +
                    " id in rental point by: " + rentalPointId + " id");
            throw new EnterErroneousDataException("this rental point does not support subscription payment");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Membership> findAllMembershipsInRentalPoint(Long rentalPointId) {
        RentalPoint rentalPoint = rpService.findById(rentalPointId);
        log.info("memberships by rental point: " + rentalPointId + " id successfully find");
        return membershipRepository.findMembershipByRentalPointAndMembershipStatus(rentalPoint,
                MembershipStatusInRentalPoint.AVAILABLE);
    }

    @Override
    @Transactional
    public void deleteMembershipFromRentalPoint(Long rentalPointId, Long membershipId) {
        RentalPoint rentalPoint = rpService.findById(rentalPointId);
        log.info("membership deletion begins by: " + membershipId +
                " id from rental point by: " + rentalPointId + " id");
        if (rentalPoint.getMemberships() != null) {
            Membership membership = membershipRepository
                    .findMembershipByRentalPointIdAndId(rentalPointId, membershipId)
                    .orElseThrow(() -> new NoDataFoundException("membership by: "
                            + membershipId + " id in rental point by: "
                            + rentalPointId + " id not found"));
            if (!MembershipStatusInRentalPoint.DELETED.equals(membership.getMembershipStatus())) {
                membership.setMembershipStatus(MembershipStatusInRentalPoint.DELETED);
                membershipRepository.save(membership);
                log.info("membership successfully deleted by: " + membershipId +
                        " id from rental point by: " + rentalPointId + " id");
            } else {
                throw new EnterErroneousDataException("membership already deleted or not found by: "
                        + membershipId + " id");
            }
        } else {
            throw new NoDataFoundException("memberships in rental point by: "
                    + rentalPointId + " is empty");
        }
    }

    @Override
    @Transactional
    public Membership findMembershipByIdAndRentalPointIdAndMembershipStatus(Long membershipId, Long rentalPointId) {
        log.info("finding membership by: " + membershipId +
                " id and rental point: " + rentalPointId + " id being started");
        return membershipRepository.findMembershipByIdAndRentalPointIdAndMembershipStatus(membershipId,
                        rentalPointId,
                        MembershipStatusInRentalPoint.AVAILABLE)
                .orElseThrow(() -> new NoDataFoundException("membership by: " + membershipId +
                        " not found in rental point by: " + rentalPointId + " id"));
    }
}
