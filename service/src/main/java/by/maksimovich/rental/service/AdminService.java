package by.maksimovich.rental.service;

/**
 * @author Maksim Maksimovich
 */
public interface AdminService {
    /**
     * Выдача прав менеджера
     *
     * @param username имя пользователя, которому будет выдана роль
     */
    void issuanceManagerRights(String username);

    /**
     * Удаление прав менеджера
     *
     * @param username имя пользователя, у которого будет удалена роль
     */
    void removeManagerRights(String username);
}