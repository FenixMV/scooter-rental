package by.maksimovich.rental.service;

import by.maksimovich.rental.entity.Model;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
public interface ModelService {
    /**
     * Создание модели электросамоката
     *
     * @param model объект для создания модели
     */
    void create(Model model);

    /**
     * Удаление необходимой модели, при удалении модели электросамоката она будет помечена как "не выпускается"
     * И в дальнейшем создание электросамоката под такой моделью не представляется возможнымм
     *
     * @param modelId уникальный ключ модели электросамоката
     */
    void delete(Long modelId);

    /**
     * Обновление уже существующей модели электросамоката
     *
     * @param model модель электросамоката для обновления
     */
    void update(Model model);

    /**
     * Поиск всех моделей электросамокатов, в том числе и тех, которые уже не выпускаются
     *
     * @return список моделей электросамокатов
     */
    List<Model> findAll();

    /**
     * Поиск модели по ключу
     *
     * @param modelId ключ для поиска конкретной модели электросамоката
     * @return модель электросамоката
     */
    Model findById(Long modelId);
}
