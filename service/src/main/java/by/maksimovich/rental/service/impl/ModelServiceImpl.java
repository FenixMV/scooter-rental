package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.ModelRepository;
import by.maksimovich.rental.entity.Model;
import by.maksimovich.rental.entity.ModelStatus;
import by.maksimovich.rental.service.ModelService;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ModelServiceImpl implements ModelService {
    private final ModelRepository modelRepository;

    @Override
    @Transactional
    public void create(Model model) {
        if (modelRepository.findByModification(model.getModification()).isEmpty()) {
            modelRepository.save(model);
            log.info("model successfully saved");
        } else {
            log.info("model doesn't saved");
            throw new EnterErroneousDataException("such a model already exists");
        }
    }

    @Override
    @Transactional
    public void delete(Long modelId) {
        Model model = modelRepository.findById(modelId)
                .orElseThrow(() -> new NoDataFoundException("model not found by: " + modelId + " id"));
        if (!ModelStatus.NOT_PRODUCED.equals(model.getModelStatus())) {
            model.setModelStatus(ModelStatus.NOT_PRODUCED);
            modelRepository.save(model);
            log.info("model by: " + modelId + " successfully deleted");
        } else {
            log.info("model by: " + modelId + " doesn't deleted");
            throw new EnterErroneousDataException("Incorrect data entry");
        }
    }

    @Override
    @Transactional
    public void update(Model model) {
        if (modelRepository.findByModification(model.getModification()).isEmpty()) {
            modelRepository.save(model);
            log.info("model by: " + model.getId() + " successfully updated");
        } else {
            log.info("model by: " + model.getId() + " doesn't updated");
            throw new EnterErroneousDataException("such a model already exists");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Model> findAll() {
        log.info("find all models");
        return modelRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Model findById(Long modelId) {
        log.info("electric scooter search by: " + modelId + " id");
        return modelRepository.findById(modelId)
                .orElseThrow(() -> new NoDataFoundException("model not found by: " + modelId + " id"));
    }
}
