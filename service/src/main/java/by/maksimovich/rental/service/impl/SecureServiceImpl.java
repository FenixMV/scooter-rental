package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.service.SecureService;
import by.maksimovich.rental.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maksim Maksimovich
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SecureServiceImpl implements SecureService {
    private final UserService userService;

    @Override
    @Transactional(readOnly = true)
    public Long getIdCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        log.info("get id current user finished successfully");
        return userService.findByUsername(user.getUsername()).getId();
    }
}
