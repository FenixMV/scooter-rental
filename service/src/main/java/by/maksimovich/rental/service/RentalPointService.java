package by.maksimovich.rental.service;

import by.maksimovich.rental.entity.RentalPoint;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
public interface RentalPointService {

    /**
     * Создание точки проката электросамокатов
     *
     * @param rentalPoint точка проката, которая будет создана
     */
    void create(RentalPoint rentalPoint);

    /**
     * Обновление информации о точке проката электросамокатов
     *
     * @param rentalPoint точка проката, которая будет обновлена
     */
    void update(RentalPoint rentalPoint);

    /**
     * Удаление точки проката по id
     *
     * @param id уникальный ключ искомой точки проката
     */
    void delete(Long id);

    /**
     * @return Показ всех доступных точек проката электросамокатов
     */
    List<RentalPoint> showMapWithAvailableRentalPoints();

    /**
     * @return Показ всех точек проката электро самокатов
     */
    List<RentalPoint> showMapWithAllRentalPoints();

    /**
     * @param id уникальный ключ для поиска точки проката
     * @return возвращает точку проката, согласно уникальному ключу
     */
    RentalPoint findById(Long id);

    /**
     * Добавление нового способа оплаты зазака в точке проката электросамокатов
     *
     * @param id              уникальный ключ точки проката, в которую будет добавлен новый метод оплаты
     * @param paymentTypeName новый метод оплаты
     */
    void addNewPaymentType(Long id, String paymentTypeName);

    /**
     * Удаление способа оплаты из точки проката электросамокатов
     *
     * @param id              уникальный ключ точки проката, из которой будет удален метод оплаты
     * @param paymentTypeName метод оплаты, который будет удален из точки проката
     */
    void removePaymentType(Long id, String paymentTypeName);
}
