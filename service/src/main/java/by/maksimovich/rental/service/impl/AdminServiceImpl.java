package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.entity.Role;
import by.maksimovich.rental.entity.User;
import by.maksimovich.rental.service.AdminService;
import by.maksimovich.rental.service.RoleService;
import by.maksimovich.rental.service.UserService;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {
    private final UserService userService;
    private final RoleService roleService;

    @Override
    public void issuanceManagerRights(String username) {
        User user = userService.findByUsername(username);
        List<Role> roles = user.getRoles();
        Role roleManagerToGetUser = roleService.findByName("ROLE_MANAGER");
        if (roles.stream().noneMatch(role -> "ROLE_MANAGER".equals(role.getName()))) {
            roles.add(roleManagerToGetUser);
            userService.updateRoles(username, roles);
            log.info("the role by: " + roleManagerToGetUser.getName() +
                    " name was successfully assigned to a user with: " + username + " username");
        } else {
            log.info("the user already has such a role in the system");
            throw new EnterErroneousDataException("the user: " + username +
                    " by username already has such a role: " + roleManagerToGetUser.getName() + " in the system");
        }
    }

    @Override
    public void removeManagerRights(String username) {
        User user = userService.findByUsername(username);
        List<Role> roles = user.getRoles();
        Role roleToRemove = roles.stream().filter(role -> "ROLE_MANAGER".equals(role.getName())).findFirst()
                .orElseThrow(() -> new NoDataFoundException("the user with: " + username +
                        " username does not have such a role with"));
        roles.remove(roleToRemove);
        userService.updateRoles(username, roles);
        log.info("manager role successfully deleted");
    }
}
