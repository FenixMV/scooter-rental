package by.maksimovich.rental.service;

import by.maksimovich.rental.entity.Role;
import by.maksimovich.rental.entity.User;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
public interface UserService {
    /**
     * Сохранение нового пользователя в системе
     *
     * @param user пользователь, который будет сохранен
     */
    User register(User user);

    /**
     * Обновление личной информации клиента
     *
     * @param userId    уникальный ключ пользователя
     * @param lastname  новое значение фамилии
     * @param firstname новое значение имени
     */
    void updateInfo(Long userId, String lastname, String firstname);

    /**
     * Обновление списка ролей пользователя
     *
     * @param username пользователь, которому будут добавлены новые(ая) роли(ь)
     * @param roles    список ролей, которые будут внедрены
     */
    void updateRoles(String username, List<Role> roles);

    /**
     * Удаление клиента из системы, при удалении пользователя, запись о его существовании остается,
     * одннако, статус его профиля изменяется на "DELETED", такой пользователь в дальнейшем не имеет
     * возможности осуществлять заказы
     *
     * @param userId ключ пользователя, который будет удален
     */
    void delete(Long userId);

    /**
     * Поиск пользователя по его никнейму
     *
     * @param username уникальное имя принадлежащее исключительно одному конкретному пользователю
     * @return пользователь, который успешно найден в результате поиска
     */
    User findByUsername(String username);

    /**
     * Поиск пользователя по ключу
     *
     * @param userId ключ пользователя
     * @return пользователь, который успешно найден в результате поиска
     */
    User findById(Long userId);

    /**
     * Покупка абонемента в точке проката электросамокатов
     *
     * @param userId        уникальный ключ клиента, который осуществляет покупку абонемента
     * @param rentalPointId точка проката, в которой происходит покупка абонемента
     * @param membershipId  абонемент, который потенциально будет куплен
     */
    void buyMembership(Long userId, Long rentalPointId, Long membershipId);

    /**
     * Изменение пароля пользователя
     *
     * @param userId   уникальный ключ пользователя, у которого будет изменен пароль
     * @param password новый пароль, который будет изменен
     */
    void changePassword(Long userId, String password);

    /**
     * Просмотор своей личной информации
     *
     * @param userId уникальный ключ пользователя у которого будет выведена информация об аккаунте
     * @return полная информация, о данных владельца аккаутна, список его ролей в системе и список его абонементов
     */
    User showMyInfo(Long userId);
}
