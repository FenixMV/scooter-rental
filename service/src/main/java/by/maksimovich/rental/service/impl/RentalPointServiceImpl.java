package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.PaymentTypeRepository;
import by.maksimovich.rental.RentalPointRepository;
import by.maksimovich.rental.entity.*;
import by.maksimovich.rental.service.RentalPointService;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class RentalPointServiceImpl implements RentalPointService {
    private final RentalPointRepository rentalPointRepository;
    private final PaymentTypeRepository paymentTypeRepository;

    @Override
    @Transactional
    public void create(RentalPoint rentalPoint) {
        rentalPointRepository.save(rentalPoint);
        log.info("rental point successfully saved by: " + rentalPoint.getId() + " id");
    }

    @Override
    @Transactional
    public void update(RentalPoint rentalPoint) {
        rentalPointRepository.findById(rentalPoint.getId())
                .orElseThrow(() -> new NoDataFoundException("rental point not found by: " + rentalPoint.getId() + " id"));
        rentalPointRepository.save(rentalPoint);
        log.info("rental point successfully updated by: " + rentalPoint.getId() + " id");
    }

    @Override
    @Transactional
    public void delete(Long id) {
        RentalPoint rentalPoint = rentalPointRepository.findById(id)
                .orElseThrow(() -> new NoDataFoundException("rental point not found by: " + id + " id"));
        checkRentalPointForStatusAndActiveOrders(rentalPoint);
        rentalPoint.setRentalPointStatus(RentalPointStatus.DELETED);
        Set<Scooter> scooters = rentalPoint.getScooters();
        scooters.forEach(scooter -> scooter.setRentalPoint(null));
        rentalPointRepository.save(rentalPoint);
        log.info("rental point successfully deleted by: " + id + " id");
    }

    @Override
    @Transactional(readOnly = true)
    public List<RentalPoint> showMapWithAvailableRentalPoints() {
        log.info("searching for available rental points");
        List<RentalPoint> rentalPoints = rentalPointRepository
                .findRentalPointsByRentalPointStatus(RentalPointStatus.ACTIVE);
        rentalPoints.forEach(rentalPoint -> rentalPoint.setMemberships(rentalPoint.getMemberships()
                .stream()
                .filter(membership -> membership.getMembershipStatus().equals(MembershipStatusInRentalPoint.AVAILABLE))
                .collect(Collectors.toList())));
        return rentalPoints;
    }

    @Override
    @Transactional(readOnly = true)
    public List<RentalPoint> showMapWithAllRentalPoints() {
        log.info("searching all rental points");
        return rentalPointRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public RentalPoint findById(Long id) {
        log.info("searching rental point by: " + id);
        return rentalPointRepository.findById(id)
                .orElseThrow(() -> new NoDataFoundException("rental point not found by: " + id + " id"));
    }

    @Override
    @Transactional
    public void addNewPaymentType(Long id, String paymentTypeName) {
        RentalPoint rentalPoint = rentalPointRepository.findById(id)
                .orElseThrow(() -> new NoDataFoundException("rental point not found by: " + id + " id"));
        Set<PaymentType> paymentTypes = rentalPoint.getPaymentTypes();
        log.info("adding new payment type: " + paymentTypeName + " in rental point by: " + id + " id get started");
        if (paymentTypes.stream()
                .noneMatch(paymentTypeInRp -> paymentTypeInRp.getType().equals(paymentTypeName))) {
            PaymentType newPaymentType = new PaymentType();
            newPaymentType.setType(paymentTypeName);
            paymentTypeRepository.save(newPaymentType);
            PaymentType paymentType = paymentTypeRepository.findPaymentTypeByType(paymentTypeName)
                    .orElseThrow(() -> new NoDataFoundException("payment type: " + paymentTypeName + " not found"));
            paymentTypes.add(paymentType);
            rentalPointRepository.save(rentalPoint);
            log.info("payment type: " + paymentTypeName + " successfully added to rental point by: " + id);
        } else {
            log.info("this payment method: " + paymentTypeName + " already exists");
            throw new EnterErroneousDataException("this payment method: " + paymentTypeName + " already exists");
        }
    }

    @Override
    @Transactional
    public void removePaymentType(Long id, String paymentTypeName) {
        RentalPoint rentalPoint = rentalPointRepository.findById(id)
                .orElseThrow(() -> new NoDataFoundException("rental point not found by: " + id + " id"));
        PaymentType paymentType = paymentTypeRepository.findPaymentTypeByType(paymentTypeName)
                .orElseThrow(() -> new NoDataFoundException("payment type: " + paymentTypeName + " not found"));
        Set<PaymentType> paymentTypes = rentalPoint.getPaymentTypes();
        if (paymentTypes.stream()
                .anyMatch(paymentTypeInRp -> paymentTypeInRp.getType().equals(paymentTypeName))) {
            paymentTypes.remove(paymentType);
            rentalPointRepository.save(rentalPoint);
            log.info("payment type: " + paymentTypeName + " successfully removed from rental point by: " + id);
        } else {
            log.info("there is no such payment method");
            throw new EnterErroneousDataException("there is no such payment method");
        }
    }

    private void checkRentalPointForStatusAndActiveOrders(RentalPoint rentalPoint) {
        if (RentalPointStatus.DELETED.equals(rentalPoint.getRentalPointStatus())) {
            log.info("there is no such rental point or it has already been deleted");
            throw new EnterErroneousDataException("there is no such rental point or it has already been deleted");
        }
        if (rentalPoint.getScooters().stream()
                .anyMatch(scooter -> ScooterOrderStatus.UNAVAILABLE.equals(scooter.getAvailabilityStatus()))) {
            log.info("there are unfinished orders left at this rental point");
            throw new EnterErroneousDataException("there are unfinished orders left at this rental point");
        }
    }
}
