package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.RoleRepository;
import by.maksimovich.rental.entity.Role;
import by.maksimovich.rental.service.RoleService;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maksim Maksimovich
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Override
    @Transactional
    public void createNewRole(String name) {
        if (roleRepository.findByName(name).isEmpty()){
            Role role = new Role();
            role.setName(name);
            roleRepository.save(role);
            log.info("new role by: " + name + " successfully created");
        } else {
            log.info("role: " + name + " already exists");
            throw new EnterErroneousDataException("such a role already exists in the system");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Role findByName(String name) {
        log.info("find role by: " + name + " name has begun");
        return roleRepository.findByName(name)
                .orElseThrow(() -> new NoDataFoundException("role not found by: " + name + " name"));
    }
}
