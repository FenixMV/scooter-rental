package by.maksimovich.rental.service;

import by.maksimovich.rental.entity.Role;

/**
 * @author Maksim Maksimovich
 */
public interface RoleService {
    /**
     * Создание новой роли в системе
     *
     * @param name имя новой роли
     */
    void createNewRole(String name);

    /**
     * Поиск роли по имени
     *
     * @param name имя роли
     * @return роль, которая будет найдена по имени
     */
    Role findByName(String name);
}
