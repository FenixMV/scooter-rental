package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.RoleRepository;
import by.maksimovich.rental.UserRepository;
import by.maksimovich.rental.entity.*;
import by.maksimovich.rental.kafka.service.MailSenderService;
import by.maksimovich.rental.service.MembershipService;
import by.maksimovich.rental.service.UserService;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Maksim Maksimovich
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final MembershipService membershipService;
    private final MailSenderService mailSenderService;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public User register(User user) {
        Role roleUser = roleRepository.findByName("ROLE_USER")
                .orElseThrow(() -> new NoDataFoundException("role not found "));
        List<Role> userRoles = new ArrayList<>();
        checkUserOnUniqData(user);
        userRoles.add(roleUser);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(userRoles);
        user.setStatus(UserStatus.ACTIVE);
        User registeredUser = userRepository.save(user);
        mailSenderService.sendNotificationAboutUserRegistration(user.getUsername(), user.getEmail());
        log.info("IN register - user: {} successfully registered", registeredUser);
        return registeredUser;
    }

    @Override
    @Transactional
    public void updateInfo(Long userId, String lastname, String firstname) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NoDataFoundException("User not found by: " + userId + " id"));
        if (firstname != null && lastname != null) {
            user.setUpdated(LocalDateTime.now());
            user.setLastname(lastname);
            user.setFirstname(firstname);
            userRepository.save(user);
            log.info("user successfully updated by: " + user.getId() + " id");
        }
        log.info("user doesn't updated");
    }

    @Override
    @Transactional
    public void updateRoles(String username, List<Role> roles) {
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new NoDataFoundException("User not found by: " + username + " username"));
        user.setRoles(roles);
        userRepository.save(user);
        log.info("user roles successfully updated");
    }

    @Override
    @Transactional
    public void delete(Long userId) {
        log.info("the deleting user operations has begun");
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NoDataFoundException("User not found by: " + userId + " id"));
        if (!UserStatus.DELETED.equals(user.getStatus())) {
            user.setStatus(UserStatus.DELETED);
            List<Order> openUserOrders = user.getOrders().stream()
                    .filter(order -> OrderStatus.OPEN.equals(order.getOrderStatus()))
                    .collect(Collectors.toList());
            openUserOrders.forEach(order -> order.setOrderStatus(OrderStatus.FINISHED));
            userRepository.save(user);
            log.info("user successfully deleted by: " + userId);
        } else {
            log.info("user not deleted by: " + userId + " id");
            throw new EnterErroneousDataException("Incorrect data entry");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public User findByUsername(String username) {
        log.info("find user by: " + username + " username started");
        return userRepository.findUserByUsername(username)
                .orElseThrow(() -> new NoDataFoundException("user not found by: " + username + " username"));
    }

    @Override
    @Transactional(readOnly = true)
    public User findById(Long userId) {
        log.info("find user by: " + userId + " id started");
        return userRepository.findById(userId)
                .orElseThrow(() -> new NoDataFoundException("User not found by: " + userId + " id"));
    }

    @Override
    @Transactional
    public void buyMembership(Long userId, Long rentalPointId, Long membershipId) {
        log.info("the membership purchase operation has begun");
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NoDataFoundException("User not found by: " + userId + " id"));
        List<Membership> memberships = user.getMemberships();
        Membership membershipFromRp = membershipService
                .findMembershipByIdAndRentalPointIdAndMembershipStatus(membershipId, rentalPointId);
        membershipFromRp.setUser(user);
        membershipFromRp.setPurchaseTime(LocalDateTime.now());
        memberships.add(membershipFromRp);
        membershipService.update(membershipFromRp);
        log.info("membership successfully purchased by: " + membershipId +
                " id in rental point by: " + rentalPointId + " id");
    }

    @Override
    @Transactional
    public void changePassword(Long userId, String password) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NoDataFoundException("User not found by: " + userId + " id"));
        user.setPassword(passwordEncoder.encode(password));
        user.setUpdated(LocalDateTime.now());
        userRepository.save(user);
        log.info("password successfully changed");
    }

    @Override
    public User showMyInfo(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new NoDataFoundException("User not found by: " + userId + " id"));
    }

    private void checkUserOnUniqData(User user) {
        if (userRepository.findUserByUsername(user.getUsername()).isPresent()
                && userRepository.findUserByEmail(user.getEmail()).isPresent()
                && userRepository.findUserByPhoneNumber(user.getPhoneNumber()).isPresent()) {
            log.info("user data doesn't uniq");
            throw new EnterErroneousDataException("user data doesn't uniq");
        }
    }
}
