package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.ScooterRepository;
import by.maksimovich.rental.entity.*;
import by.maksimovich.rental.service.ModelService;
import by.maksimovich.rental.service.RentalPointService;
import by.maksimovich.rental.service.ScooterService;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ScooterServiceImpl implements ScooterService {
    private final ScooterRepository scooterRepository;
    private final RentalPointService rentalPointService;
    private final ModelService modelService;

    @Override
    @Transactional
    public void save(Scooter scooter, Long modelId) {
        Model model = modelService.findById(modelId);
        if (ModelStatus.PRODUCED.equals(model.getModelStatus()) && scooter.getRentalPoint() == null) {
            scooter.setModel(model);
            scooterRepository.save(scooter);
            log.info("scooter by: " + scooter.getId() +
                    " id and model: " + modelId + " id successfully saved");
        } else {
            log.info("scooter by: " + scooter.getId() +
                    " id and model: " + modelId +
                    " id doesn't saved because electric scooter is no longer serviced or this model doesn't produced now");
            throw new EnterErroneousDataException("Incorrect data entry");
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Scooter scooter = scooterRepository.findById(id)
                .orElseThrow(() -> new NoDataFoundException("scooter not found by: " + id + " id"));
        if (ScooterOrderStatus.AVAILABLE.equals(scooter.getAvailabilityStatus())) {
            scooterRepository.delete(scooter);
            log.info("scooter by: " + scooter.getId() + " id successfully deleted");
        } else {
            throw new EnterErroneousDataException("the electric scooter is currently rented or does not exist");
        }
    }

    @Override
    @Transactional
    public void update(Scooter scooter) {
        if (ModelStatus.PRODUCED.equals(scooter.getModel().getModelStatus())) {
            scooterRepository.save(scooter);
            log.info("scooter by: " + scooter.getId() + " id successfully updated");
        } else {
            throw new EnterErroneousDataException("the electric scooter is no longer serviced");
        }
    }

    @Override
    @Transactional
    public void saveScooterInRentalPoint(Long scooterId, Long rentalPointId) {
        log.info("save scooter by: " + scooterId +
                " id in rental point by: " + rentalPointId + " id get started");
        Scooter scooter = scooterRepository.findById(scooterId)
                .orElseThrow(() -> new NoDataFoundException("scooter not found by " + scooterId + " id"));
        RentalPoint rentalPoint = rentalPointService.findById(rentalPointId);
        if (scooter.getRentalPoint() == null) {
            if (rentalPoint.getScootersCapacity() > rentalPoint.getScooters().size()) {
                scooter.setRentalPoint(rentalPoint);
                scooterRepository.save(scooter);
                log.info("scooter by: " + scooterId +
                        " id successfully saved in rental point by: " + rentalPointId + " id");
            } else {
                log.info("scooter by: " + scooterId +
                        " id not saved in rental point by: " + rentalPointId +
                        " id because rental point capacity is full");
                throw new EnterErroneousDataException("Incorrect data entry");
            }
        } else {
            log.info("scooter by: " + scooterId +
                    " id not saved in rental point by: " + rentalPointId +
                    " id because this scooter already present in rental point");
            throw new EnterErroneousDataException("the scooter by: " + scooterId
                    + " id is already present in rental point");
        }
    }

    @Override
    @Transactional
    public void deleteScooterInRentalPoint(Long rentalPointId, Long scooterId) {
        log.info("delete scooter by: " + scooterId +
                " id from rental point by: " + rentalPointId + " id get started");
        Scooter scooter = scooterRepository.findById(scooterId)
                .orElseThrow(() -> new NoDataFoundException("scooter not found by " + scooterId + " id"));
        RentalPoint rentalPoint = scooter.getRentalPoint();
        if (rentalPoint != null && rentalPoint.getScootersCapacity() > rentalPoint.getScooters().size()) {
            scooter.setRentalPoint(null);
            scooterRepository.save(scooter);
            log.info("scooter by: " + scooterId +
                    " id successfully deleted from rental point by: " + rentalPointId + " id");
        } else {
            log.info("scooter by: " + scooterId +
                    " id not deleted from rental point by: " + rentalPointId + " id");
            throw new EnterErroneousDataException("Incorrect data entry");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Scooter> findAll() {
        log.info("find scooters get started");
        return scooterRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Scooter> findByModelName(String model) {
        log.info("find scooters by: " + model + " model started");
        return scooterRepository.findByModelName(model);
    }

    @Override
    @Transactional(readOnly = true)
    public Scooter findById(Long scooterId) {
        log.info("find scooter by: " + scooterId + " id started");
        return scooterRepository.findById(scooterId)
                .orElseThrow(() -> new NoDataFoundException("scooter not found by " + scooterId + " id"));
    }
}
