package by.maksimovich.rental.service;

/**
 * @author Maksim Maksimovich
 */
public interface SecureService {

    /**
     * Получение id авторизованного пользователя
     *
     * @return id
     */
    Long getIdCurrentUser();
}
