package by.maksimovich.rental.service;

import by.maksimovich.rental.entity.Scooter;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
public interface ScooterService {

    /**
     * Сохранение электросамоката
     *
     * @param scooter самокат для сохранения
     */
    void save(Scooter scooter, Long modelId);

    /**
     * Удаление электросамоката
     *
     * @param id уникальный ключ для поиска электросамоката
     */
    void delete(Long id);

    /**
     * Обновление информации о электросамокате
     *
     * @param scooter электросамокат для обновления
     */
    void update(Scooter scooter);

    /**
     * Сохранение электросамоката в точку проката
     *
     * @param scooterId     уникальный ключ электросамоката для добавления
     * @param rentalPointId уникальный ключ точки проката для нахождения места,
     *                      куда будет добавлен электросамокат
     */
    void saveScooterInRentalPoint(Long scooterId, Long rentalPointId);

    /**
     * Удаление электросамоката из точки проката
     *
     * @param rentalPointId уникальный ключ для поиска точки проката,
     *                      откуда будет удален электросамокат
     * @param scooterId     уникальный ключ для поиска электросамоката
     */
    void deleteScooterInRentalPoint(Long rentalPointId, Long scooterId);

    /**
     * Поиск всех электросамокатов
     *
     * @return список электросамокатов
     */
    List<Scooter> findAll();

    /**
     * Поиск электросамоката по имени модели
     *
     * @param model имя модели для поиска
     * @return список всех самокатов имеющих заданную модель
     */
    List<Scooter> findByModelName(String model);

    /**
     * Поиск электросамоката по id
     *
     * @param scooterId уникальный ключ для поиска
     * @return электросамокат, если был найден
     */
    Scooter findById(Long scooterId);
}
