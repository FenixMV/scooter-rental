package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.OrderRepository;
import by.maksimovich.rental.ScooterRepository;
import by.maksimovich.rental.entity.*;
import by.maksimovich.rental.service.*;
import exception.EnterErroneousDataException;
import exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.DAYS;

/**
 * @author Maksim Maksimovich
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final ScooterService scooterService;
    private final ScooterRepository scooterRepository;
    private final UserService userService;
    private final RentalPointService rentalPointService;
    private final MembershipService membershipService;

    @Override
    @Transactional
    public void save(Order order) {
        orderRepository.save(order);
        log.info("order successfully saved");
    }

    @Override
    @Transactional
    public void delete(Long orderId) {
        log.info("operations for delete order by: " + orderId + " id get started");
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new NoDataFoundException("order not found by: " + orderId + " id"));
        if (!OrderStatus.DELETED.equals(order.getOrderStatus())) {
            order.setOrderStatus(OrderStatus.DELETED);
            Scooter scooter = order.getScooter();
            scooter.setAvailabilityStatus(ScooterOrderStatus.AVAILABLE);
            scooterService.update(scooter);
            orderRepository.save(order);
            log.info("order by: " + orderId + " id successfully deleted");
        } else {
            log.info("order by: " + orderId + " id doesn't deleted");
            throw new EnterErroneousDataException("Incorrect data entry");
        }
    }

    @Override
    @Transactional
    public void makeOrder(Long userId, Long rentalPointId, Long scooterId, Long membershipId) {
        log.info("order is started...");
        Scooter scooter = scooterRepository
                .findByIdAndRentalPointIdAndAvailabilityStatus(scooterId, rentalPointId, ScooterOrderStatus.AVAILABLE)
                .orElseThrow(() -> new EnterErroneousDataException("this scooter is currently occupied or does not exist at all"));
        checkRentalPointStatusOnActive(scooter);
        User user = userService.findById(userId);
        checkUserForOpenOrder(user);
        checkForHaveEnoughPowerReserve(scooter);
        RentalPoint rentalPoint = rentalPointService.findById(rentalPointId);
        if (membershipId != null && user.getMemberships()
                .stream()
                .anyMatch(membership -> membership.getId().equals(membershipId)
                        && MembershipStatusInRentalPoint.AVAILABLE.equals(membership.getMembershipStatus()))
                && rentalPoint.getPaymentTypes()
                .stream()
                .anyMatch(paymentType -> paymentType.getType().equals("membership"))) {
            Membership membership = membershipService.findById(membershipId);
            Order orderWithMembership = createOrderWithMembership(scooter, user, membership);
            orderRepository.save(orderWithMembership);
            membership.setActivationTime(LocalDateTime.now());
            log.info("order successfully made with membership by order id: " + orderWithMembership.getId());
        } else {
            Order order = createOrderWithoutMembership(scooter, user);
            orderRepository.save(order);
            log.info("order successfully made without membership by order id: " + order.getId());
        }
        scooter.setAvailabilityStatus(ScooterOrderStatus.UNAVAILABLE);
        scooterService.update(scooter);
    }

    @Override
    @Transactional
    public void cancelOrder(Long userId, Long orderId) {
        Order userOrder = orderRepository.findOrderByIdAndUserId(orderId, userId)
                .orElseThrow(() -> new NoDataFoundException("order not found by: " + orderId + " id"));
        checkUserOrderForOpenStatus(userOrder);
        userOrder.setOrderStatus(OrderStatus.CANCELED);
        Scooter scooter = userOrder.getScooter();
        scooter.setAvailabilityStatus(ScooterOrderStatus.AVAILABLE);
        scooterService.update(scooter);
        orderRepository.save(userOrder);
        log.info("order by: " + orderId + " successfully canceled by user: " + userId + " id");
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> showScooterOrderHistory(Long scooterId) {
        List<Order> ordersByScooterId = orderRepository.findOrdersByScooterId(scooterId);
        if (!ordersByScooterId.isEmpty()) {
            log.info("scooter order history scooter by: " + scooterId + " id");
            return ordersByScooterId;
        } else {
            throw new NoDataFoundException("no orders have been found for this scooter");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> showScooterOrderHistoryByUser(String username) {
        log.info("scooter order history scooter by: " + username + " username");
        return orderRepository.findOrdersByUserUsername(username);
    }

    @Override
    @Transactional
    public void finishOrder(Long userId, Long orderId) {
        log.info("finish order by: " + orderId +
                " order id and user id: " + userId + " get started");
        Order userOrder = orderRepository.findOrderByIdAndUserId(orderId, userId)
                .orElseThrow(() -> new NoDataFoundException("order not found by: " + orderId + " id"));
        checkUserOrderForOpenStatus(userOrder);
        userOrder.setOrderStatus(OrderStatus.FINISHED);
        userOrder.getScooter().setAvailabilityStatus(ScooterOrderStatus.AVAILABLE);
        userOrder.setOrderFinishTime(LocalDateTime.now());
        if (userOrder.getMembership() != null) {
            Membership membership = userOrder.getMembership();
            membership.setExpirationTime(LocalDateTime.now());
            if (membership.getActionTime() > calculateMembershipTime(membership.getActivationTime(),
                    membership.getExpirationTime())) {
                membership.setMembershipStatus(MembershipStatusInRentalPoint.FINISHED);
                long difBetweenFinishAndStartOrderTime = getDifFromTwoTimes(userOrder.getOrderTime(),
                        userOrder.getOrderFinishTime());
                if (userOrder.getMileage() == null) {
                    userOrder.setMileage(BigDecimal.valueOf(difBetweenFinishAndStartOrderTime));
                } else {
                    userOrder.setMileage(userOrder.getMileage().add(BigDecimal.valueOf(difBetweenFinishAndStartOrderTime)));
                }
                orderRepository.save(userOrder);
                log.info("order successfully finished by membership: " + membership.getName() + " name");
            } else {
                userOrder.setPaymentMethod("cash");
                userOrder.setMembership(null);
                finishOrderByCash(userOrder);
                orderRepository.save(userOrder);
                log.info("order by: " + orderId +
                        " successfully finished by cash on total price: " + userOrder.getTotalPrice());
            }
        } else {
            finishOrderByCash(userOrder);
            orderRepository.save(userOrder);
            log.info("order by: " + orderId +
                    " successfully finished by cash on total price: " + userOrder.getTotalPrice());
        }
    }

    private void finishOrderByCash(Order userOrder) {
        BigDecimal totalOrderPrice = calculateOrderPrice(userOrder.getOrderTime(), userOrder.getOrderFinishTime(),
                userOrder.getScooter().getPricePerMinute());
        userOrder.setTotalPrice(totalOrderPrice);
        userOrder.setMileage(BigDecimal.valueOf(DAYS.toChronoUnit()
                .between(userOrder.getOrderTime(), userOrder.getOrderFinishTime())));
        orderRepository.save(userOrder);
        log.info("order by: " + userOrder.getId() + " successfully finished by cash");
    }

    private void checkRentalPointStatusOnActive(Scooter scooter) {
        if (scooter.getRentalPoint().getRentalPointStatus() != RentalPointStatus.ACTIVE) {
            log.info("Rental point status is not ACTIVE");
            throw new RuntimeException("RentalPointStatus is not ACTIVE");
        }
    }

    private Order createOrderWithMembership(Scooter scooter, User user, Membership membership) {
        Order order = new Order();
        order.setOrderTime(LocalDateTime.now());
        order.setOrderStatus(OrderStatus.OPEN);
        order.setUser(user);
        order.setMembership(membership);
        order.setScooter(scooter);
        order.setPaymentMethod("membership");
        log.info("order created with membership");
        return order;
    }

    private Order createOrderWithoutMembership(Scooter scooter, User user) {
        Order order = new Order();
        order.setOrderStatus(OrderStatus.OPEN);
        order.setOrderTime(LocalDateTime.now());
        order.setUser(user);
        order.setScooter(scooter);
        order.setPaymentMethod("cash");
        order.setMembership(null);
        log.info("order created without membership");
        return order;
    }

    private BigDecimal calculateOrderPrice(LocalDateTime startOrderTime,
                                           LocalDateTime finishOrderTime,
                                           BigDecimal pricePerMinute) {
        long dif = getDifFromTwoTimes(startOrderTime, finishOrderTime);
        return pricePerMinute.divide(new BigDecimal(60), RoundingMode.CEILING).multiply(BigDecimal.valueOf(dif));
    }

    private long getDifFromTwoTimes(LocalDateTime startOrderTime, LocalDateTime finishOrderTime) {
        long finishTime = finishOrderTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        long startTime = startOrderTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        return TimeUnit.MILLISECONDS.toSeconds(finishTime - startTime);
    }

    private long calculateMembershipTime(LocalDateTime activationTime, LocalDateTime expirationTime) {
        long finishTime = expirationTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        long startTime = activationTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        return TimeUnit.MILLISECONDS.toSeconds(finishTime - startTime);
    }

    private void checkForHaveEnoughPowerReserve(Scooter scooter) {
        if (scooter.getPowerReserve().compareTo(new BigDecimal(5)) <= 0) {
            throw new RuntimeException("checkForHaveEnoughPowerReserve");
        }
    }

    private void checkUserForOpenOrder(User user) {
        List<Order> userOrders = user.getOrders();
        userOrders.stream()
                .filter(order -> order.getOrderStatus().equals(OrderStatus.OPEN))
                .findAny()
                .ifPresent(order -> {
                    log.info("user has an open order");
                    throw new EnterErroneousDataException("the user has an open order");
                });
    }

    private void checkUserOrderForOpenStatus(Order userOrder) {
        if (!OrderStatus.OPEN.equals(userOrder.getOrderStatus())) {
            throw new EnterErroneousDataException("Incorrect data entry");
        }
    }
}
