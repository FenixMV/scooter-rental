package by.maksimovich.rental.service;

import by.maksimovich.rental.entity.Membership;

import java.util.List;

/**
 * @author Maksim Maksimovich
 */
public interface MembershipService {

    /**
     * Обновление информации абонемента
     *
     * @param membership абонемент для обновления
     */
    void update(Membership membership);

    /**
     * Поиск абонемента по ключу
     *
     * @param membershipId ключ для поиска абонемента
     * @return абонемент
     */
    Membership findById(Long membershipId);

    /**
     * Добавление абонемента в точку проката электросамокатов
     *
     * @param membership    абонемент, который будет добавлен
     * @param rentalPointId точка проката, в которую будет добавлен электросамокат
     */
    void addMembershipInRentalPoint(Membership membership, Long rentalPointId);

    /**
     * Поиск всех имеющихся и доступных абонементов в точке проката
     *
     * @param rentalPointId ключ точки проката электросамокатов для поиска всех абонементов
     * @return список всех имеющихся абонементов
     */
    List<Membership> findAllMembershipsInRentalPoint(Long rentalPointId);

    /**
     * Удаление абонемента из точки проката
     *
     * @param rentalPointId ключ точки проката, откуда будет удален абонемент
     * @param membershipId  абонемент, который будет удален из точки проката электросамокатов
     */
    void deleteMembershipFromRentalPoint(Long rentalPointId, Long membershipId);

    /**
     * Поиск абонемента в точке проката электросамокатов,
     * если абонемент в данный момент является активным для покупки
     *
     * @param membershipId  ключ абонемента по которому будет осуществлен поиск в точке проката
     * @param rentalPointId точка проката электросамокатов
     * @return доступный абонемент, который был найден по заданному ключу
     */
    Membership findMembershipByIdAndRentalPointIdAndMembershipStatus(Long membershipId, Long rentalPointId);
}
