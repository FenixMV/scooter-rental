package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.ScooterRepository;
import by.maksimovich.rental.entity.*;
import by.maksimovich.rental.service.ModelService;
import by.maksimovich.rental.service.RentalPointService;
import by.maksimovich.rental.service.ScooterService;
import exception.NoDataFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(value = MockitoExtension.class)
class ScooterServiceImplTest {

    @Mock
    private ScooterRepository scooterRepository;

    @Mock
    private RentalPointService rentalPointService;

    @Mock
    private ModelService modelService;

    private ScooterService scooterService;

    @BeforeEach
    void setup() {
        scooterService = new ScooterServiceImpl(scooterRepository, rentalPointService, modelService);
    }

    @Test
    void findByIdWhenThrowException() {
        Mockito.when(scooterRepository.findById(any()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(NoDataFoundException.class, () -> scooterService.findById(1L));
    }

    @Test
    void findAll() {
        List<Scooter> scooterList = new ArrayList<>();
        scooterList.add(new Scooter(new BigDecimal(30), new BigDecimal("0.3"), ScooterOrderStatus.AVAILABLE,
                new Model("r317", "745", new BigDecimal(30), new BigDecimal(120),
                        new BigDecimal(32), ModelStatus.PRODUCED),
                new RentalPoint("Baranovichi", "pikachu", 15, RentalPointStatus.ACTIVE,
                        63, 63, null, null, null)));
        Mockito.when(scooterRepository.findAll())
                .thenReturn(scooterList);
        List<Scooter> scooterListByModel = scooterService.findAll();
        Mockito.verify(scooterRepository)
                .findAll();
        Assertions.assertNotNull(scooterListByModel);
    }

    @Test
    void findByModelName() {
        List<Scooter> scooterList = new ArrayList<>();
        Scooter scooterFromDb = new Scooter(new BigDecimal(30), new BigDecimal("0.3"), ScooterOrderStatus.AVAILABLE,
                new Model("r317", "745", new BigDecimal(30), new BigDecimal(120),
                        new BigDecimal(32), ModelStatus.PRODUCED),
                new RentalPoint("Baranovichi", "pikachu", 15, RentalPointStatus.ACTIVE,
                        63, 63, null, null, null));
        scooterList.add(scooterFromDb);
        Mockito.when(scooterRepository.findByModelName("r317"))
                .thenReturn(scooterList);
        List<Scooter> scooterListByModel = scooterService.findByModelName("r317");
        Mockito.verify(scooterRepository)
                .findByModelName("r317");
        Assertions.assertNotNull(scooterListByModel);
    }

    @Test
    void findById() {
        Scooter scooterFromDb = new Scooter(new BigDecimal(30), new BigDecimal("0.3"), ScooterOrderStatus.AVAILABLE,
                new Model("r317", "745", new BigDecimal(30), new BigDecimal(120),
                        new BigDecimal(32), ModelStatus.PRODUCED),
                new RentalPoint("Baranovichi", "pikachu", 15, RentalPointStatus.ACTIVE,
                        63, 63, null, null, null));
        Mockito.when(scooterRepository.findById(1L))
                .thenReturn(Optional.of(scooterFromDb));
        Scooter scooterFromService = scooterService.findById(1L);
        Assertions.assertNotNull(scooterFromService);
        Assertions.assertEquals("r317", scooterFromService.getModel().getName());
    }
}