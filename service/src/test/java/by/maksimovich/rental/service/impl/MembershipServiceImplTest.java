package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.MembershipRepository;
import by.maksimovich.rental.entity.Membership;
import by.maksimovich.rental.entity.MembershipStatusInRentalPoint;
import by.maksimovich.rental.service.MembershipService;
import by.maksimovich.rental.service.RentalPointService;
import exception.NoDataFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(value = MockitoExtension.class)
class MembershipServiceImplTest {

    @Mock
    private MembershipRepository membershipRepository;

    @Mock
    private RentalPointService rentalPointService;

    private MembershipService membershipService;

    @BeforeEach
    void setup() {
        membershipService = new MembershipServiceImpl(membershipRepository, rentalPointService);
    }

    @Test
    void findById() {
        List<Membership> membershipList = new ArrayList<>();
        Membership membership = new Membership("hour of enjoy", 60, LocalDateTime.now(),
                LocalDateTime.now(), LocalDateTime.now(), MembershipStatusInRentalPoint.AVAILABLE, null, null);
        membershipList.add(membership);
        Assertions.assertNotNull(membershipList);
        Mockito.when(membershipRepository.findById(1L))
                .thenReturn(Optional.of(membership));
        Membership membershipFromService = membershipService.findById(1L);
        Assertions.assertNotNull(membershipFromService);
        Assertions.assertEquals("hour of enjoy", membershipFromService.getName());
    }

    @Test
    void findByIdWhenThrowException() {
        Mockito.when(membershipRepository.findById(any()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(NoDataFoundException.class, () -> membershipService.findById(1L));
    }
}