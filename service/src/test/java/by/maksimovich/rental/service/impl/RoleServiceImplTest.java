package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.RoleRepository;
import by.maksimovich.rental.entity.Role;
import by.maksimovich.rental.service.RoleService;
import exception.NoDataFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(value = MockitoExtension.class)
class RoleServiceImplTest {

    @Mock
    private RoleRepository roleRepository;

    private RoleService roleService;

    @BeforeEach
    void setup() {
        roleService = new RoleServiceImpl(roleRepository);
    }

    @Test
    void findByName() {
        List<Role> roleList = new ArrayList<>();
        Role roleFromDb = new Role("admin", null);
        roleList.add(roleFromDb);
        Assertions.assertNotNull(roleList);
        Mockito.when(roleRepository.findByName("admin"))
                .thenReturn(Optional.of(roleFromDb));
        Role roleFromService = roleService.findByName("admin");
        Mockito.verify(roleRepository)
                .findByName("admin");
        Assertions.assertNotNull(roleFromService);
    }

    @Test
    void findByNameWhenThrowException() {
        Mockito.when(roleRepository.findByName(any()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(NoDataFoundException.class, () -> roleService.findByName("none"));
    }
}