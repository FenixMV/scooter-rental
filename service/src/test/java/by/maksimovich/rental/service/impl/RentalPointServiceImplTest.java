package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.PaymentTypeRepository;
import by.maksimovich.rental.RentalPointRepository;
import by.maksimovich.rental.entity.RentalPoint;
import by.maksimovich.rental.entity.RentalPointStatus;
import by.maksimovich.rental.service.RentalPointService;
import exception.NoDataFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(value = MockitoExtension.class)
class RentalPointServiceImplTest {

    @Mock
    private RentalPointRepository rentalPointRepository;

    @Mock
    private PaymentTypeRepository paymentTypeRepository;

    private RentalPointService rentalPointService;

    @BeforeEach
    void setup() {
        rentalPointService = new RentalPointServiceImpl(rentalPointRepository, paymentTypeRepository);
    }

    @Test
    void create() {
        RentalPoint rentalPoint = new RentalPoint("Baranovichi", "pikachu",
                15, RentalPointStatus.ACTIVE,
                63, 63, null, null, null);
        rentalPointService.create(rentalPoint);
        Mockito.verify(
                        rentalPointRepository,
                        Mockito.times(1)
                )
                .save(rentalPoint);
    }

    @Test
    void update() {
        List<RentalPoint> rentalPointList = new ArrayList<>();
        RentalPoint rentalPointToUpdate = new RentalPoint(1L, "Baranovichi", "pikachu",
                15, RentalPointStatus.ACTIVE,
                63, 63);
        rentalPointList.add(rentalPointToUpdate);
        Assertions.assertNotNull(rentalPointList);
        Mockito.when(rentalPointRepository.findById(1L))
                .thenReturn(Optional.of(rentalPointToUpdate));
        RentalPoint rentalPointFromService = rentalPointService.findById(1L);
        rentalPointService.update(rentalPointFromService);
        Mockito.verify(
                        rentalPointRepository,
                        Mockito.times(1)
                )
                .save(rentalPointToUpdate);
    }

    @Test
    void showMapWithAvailableRentalPoints() {
        List<RentalPoint> rentalPointList = new ArrayList<>();
        rentalPointList.add(new RentalPoint("Baranovichi", "pikachu",
                15, RentalPointStatus.ACTIVE,
                63, 63, null, null, null));
        rentalPointList.add(new RentalPoint("Baranovichi", "spider",
                12, RentalPointStatus.ACTIVE,
                63, 63, null, null, null));
        Assertions.assertNotNull(rentalPointList);
        List<RentalPoint> rentalPointsAvailableList = rentalPointService.showMapWithAvailableRentalPoints();
        Assertions.assertNotNull(rentalPointsAvailableList);
        for (RentalPoint rentalPoint : rentalPointsAvailableList) {
            Assertions.assertEquals(RentalPointStatus.ACTIVE, rentalPoint.getRentalPointStatus());
        }
    }

    @Test
    void showMapWithAllRentalPoints() {
        List<RentalPoint> rentalPointList = new ArrayList<>();
        rentalPointList.add(new RentalPoint("Baranovichi", "pikachu",
                15, RentalPointStatus.ACTIVE,
                63, 63, null, null, null));
        rentalPointList.add(new RentalPoint("Baranovichi", "spider",
                12, RentalPointStatus.ACTIVE,
                63, 63, null, null, null));
        Mockito.when(rentalPointRepository.findAll())
                .thenReturn(rentalPointList);
        List<RentalPoint> mapWithAllRentalPoints = rentalPointService.showMapWithAllRentalPoints();
        Mockito.verify(rentalPointRepository)
                .findAll();
        Assertions.assertNotNull(mapWithAllRentalPoints);
    }

    @Test
    void findById() {
        List<RentalPoint> rentalPointList = new ArrayList<>();
        RentalPoint rentalPoint = new RentalPoint(1L, "Baranovichi", "pikachu",
                15, RentalPointStatus.ACTIVE,
                63, 63);
        rentalPointList.add(rentalPoint);
        Assertions.assertNotNull(rentalPointList);
        Mockito.when(rentalPointRepository.findById(1L))
                .thenReturn(Optional.of(rentalPoint));
        RentalPoint rentalPointFromService = rentalPointService.findById(1L);
        Assertions.assertNotNull(rentalPointFromService);
        Assertions.assertEquals("pikachu", rentalPointFromService.getName());
        Assertions.assertEquals("Baranovichi", rentalPointFromService.getLocation());
        Assertions.assertEquals(1L, rentalPointFromService.getId());
    }

    @Test
    void findByIdWhenThrowException() {
        Mockito.when(rentalPointRepository.findById(any()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(NoDataFoundException.class, () -> rentalPointService.findById(1L));
    }
}