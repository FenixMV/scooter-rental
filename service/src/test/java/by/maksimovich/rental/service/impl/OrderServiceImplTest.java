package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.OrderRepository;
import by.maksimovich.rental.ScooterRepository;
import by.maksimovich.rental.entity.*;
import by.maksimovich.rental.service.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@ExtendWith(value = MockitoExtension.class)
class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ScooterService scooterService;

    @Mock
    private ScooterRepository scooterRepository;

    @Mock
    private UserService userService;

    @Mock
    private RentalPointService rentalPointService;

    @Mock
    private MembershipService membershipService;

    private OrderService orderService;

    @BeforeEach
    void setup() {
        orderService = new OrderServiceImpl(orderRepository, scooterService, scooterRepository, userService, rentalPointService, membershipService);
    }

    @Test
    void save() {
        Order order = new Order(new BigDecimal(12), LocalDateTime.now(), OrderStatus.FINISHED, new BigDecimal(10), LocalDateTime.now(), "cash", new User(1L, "Maksim", "Maksimovich", "fnx", "123",
                "123@mail.by", UserStatus.ACTIVE, LocalDateTime.now(), "+375298217328"), new Scooter(new BigDecimal(30), new BigDecimal("0.3"), ScooterOrderStatus.AVAILABLE,
                new Model("r317", "745", new BigDecimal(30), new BigDecimal(120),
                        new BigDecimal(32), ModelStatus.PRODUCED),
                new RentalPoint("Baranovichi", "pikachu", 15, RentalPointStatus.ACTIVE,
                        63, 63, null, null, null)), new Membership("hour of enjoy", 60, LocalDateTime.now(),
                LocalDateTime.now(), LocalDateTime.now(), MembershipStatusInRentalPoint.AVAILABLE, null, null));
        orderService.save(order);
        Mockito.verify(
                        orderRepository,
                        Mockito.times(1)
                )
                .save(order);
    }
}