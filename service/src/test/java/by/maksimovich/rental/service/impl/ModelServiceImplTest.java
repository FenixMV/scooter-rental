package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.ModelRepository;
import by.maksimovich.rental.entity.*;
import by.maksimovich.rental.service.ModelService;
import exception.NoDataFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(value = MockitoExtension.class)
class ModelServiceImplTest {

    @Mock
    private ModelRepository modelRepository;

    private ModelService modelService;

    @BeforeEach
    void setup() {
        modelService = new ModelServiceImpl(modelRepository);
    }

    @Test
    void create() {
        Model model = new Model("xiaomi", "r317", new BigDecimal(35),
                new BigDecimal(120), new BigDecimal(32), ModelStatus.PRODUCED);
        modelService.create(model);
        Mockito.verify(
                        modelRepository,
                        Mockito.times(1)
                )
                .save(model);
    }

    @Test
    void delete() {
        List<Model> modelList = new ArrayList<>();
        Model model = new Model("xiaomi", "r317", new BigDecimal(35),
                new BigDecimal(120), new BigDecimal(32), ModelStatus.PRODUCED);
        modelList.add(model);
        Assertions.assertNotNull(modelList);
        Mockito.when(modelRepository.findById(model.getId()))
                .thenReturn(Optional.of(model));
        modelService.delete(model.getId());
        Assertions.assertEquals(ModelStatus.NOT_PRODUCED, model.getModelStatus());
    }

    @Test
    void update() {
        Model model = new Model("xiaomi", "r317", new BigDecimal(35),
                new BigDecimal(120), new BigDecimal(32), ModelStatus.PRODUCED);
        modelService.update(model);
        Mockito.verify(
                        modelRepository,
                        Mockito.times(1)
                )
                .save(model);
    }

    @Test
    void findAll() {
        List<Model> modelList = new ArrayList<>();
        Model model = new Model("xiaomi", "r317", new BigDecimal(35),
                new BigDecimal(120), new BigDecimal(32), ModelStatus.PRODUCED);
        modelList.add(model);
        Assertions.assertNotNull(modelList);
        Mockito.when(modelRepository.findAll())
                .thenReturn(modelList);
        List<Model> modelListFromService = modelService.findAll();
        Mockito.verify(modelRepository)
                .findAll();
        Assertions.assertNotNull(modelListFromService);
    }

    @Test
    void findById() {
        List<Model> modelList = new ArrayList<>();
        Model model = new Model("xiaomi", "r317", new BigDecimal(35),
                new BigDecimal(120), new BigDecimal(32), ModelStatus.PRODUCED);
        modelList.add(model);
        Assertions.assertNotNull(modelList);
        Mockito.when(modelRepository.findById(1L))
                .thenReturn(Optional.of(model));
        Model modelFromService = modelService.findById(1L);
        Assertions.assertNotNull(modelFromService);
        Assertions.assertEquals("xiaomi", modelFromService.getName());
        Assertions.assertEquals("r317", modelFromService.getModification());
    }

    @Test
    void findByIdWhenThrowException() {
        Mockito.when(modelRepository.findById(any()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(NoDataFoundException.class, () -> modelService.findById(1L));
    }
}