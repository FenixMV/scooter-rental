package by.maksimovich.rental.service.impl;

import by.maksimovich.rental.RoleRepository;
import by.maksimovich.rental.UserRepository;
import by.maksimovich.rental.entity.Role;
import by.maksimovich.rental.entity.User;
import by.maksimovich.rental.entity.UserStatus;
import by.maksimovich.rental.kafka.service.MailSenderService;
import by.maksimovich.rental.service.MembershipService;
import by.maksimovich.rental.service.UserService;
import exception.NoDataFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(value = MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private MembershipService membershipService;

    @Mock
    private MailSenderService mailSenderService;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    private UserService userService;

    @BeforeEach
    void setup() {
        userService = new UserServiceImpl(userRepository, roleRepository, membershipService, mailSenderService, passwordEncoder);
    }

    @Test
    void updateRoles() {
        List<User> userList = new ArrayList<>();
        User userFromDb = new User(1L, "Maksim", "Maksimovich", "fnx", "123",
                "123@mail.by", UserStatus.ACTIVE, LocalDateTime.now(), "+375298217328");
        userList.add(userFromDb);
        Assertions.assertNotNull(userList);
        List<Role> roles = new ArrayList<>();
        Role roleUser = new Role(1L, "ROLE_USER");
        roleRepository.save(roleUser);
        roles.add(roleUser);
        Mockito.when(userRepository.findUserByUsername("fnx"))
                .thenReturn(Optional.of(userFromDb));
        User userFromService = userService.findByUsername("fnx");
        Mockito.verify(userRepository)
                .findUserByUsername("fnx");
        userService.updateRoles(userFromService.getUsername(), roles);
        Assertions.assertEquals(roles, userFromDb.getRoles());
    }

    @Test
    void findByUsername() {
        List<User> userList = new ArrayList<>();
        User userFromDb = new User(1L, "Maksim", "Maksimovich", "fnx", "123",
                "123@mail.by", UserStatus.ACTIVE, LocalDateTime.now(), "+375298217328");
        userList.add(userFromDb);
        Assertions.assertNotNull(userList);
        Mockito.when(userRepository.findUserByUsername("fnx"))
                .thenReturn(Optional.of(userFromDb));
        User userFromService = userService.findByUsername("fnx");
        Mockito.verify(userRepository)
                .findUserByUsername("fnx");
        Assertions.assertNotNull(userFromService);
    }

    @Test
    void findByIdWhenThrowException() {
        Mockito.when(userRepository.findById(any()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(NoDataFoundException.class, () -> userService.findById(1L));
    }

    @Test
    void findByUsernameWhenThrowException() {
        Mockito.when(userRepository.findUserByUsername(any()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(NoDataFoundException.class, () -> userService.findByUsername("none"));
    }

    @Test
    void findById() {
        List<User> userList = new ArrayList<>();
        User userFromDb = new User(1L, "Maksim", "Maksimovich", "fnx", "123",
                "123@mail.by", UserStatus.ACTIVE, LocalDateTime.now(), "+375298217328");
        userList.add(userFromDb);
        Assertions.assertNotNull(userList);
        Mockito.when(userRepository.findById(1L))
                .thenReturn(Optional.of(userFromDb));
        User userFromService = userService.findById(1L);
        Mockito.verify(userRepository)
                .findById(1L);
        Assertions.assertNotNull(userFromService);
    }

    @Test
    void changePassword() {
        List<User> userList = new ArrayList<>();
        User userFromDb = new User(1L, "Maksim", "Maksimovich", "fnx", "123",
                "123@mail.by", UserStatus.ACTIVE, LocalDateTime.now(), "+375298217328");
        userList.add(userFromDb);
        Assertions.assertNotNull(userList);
        Mockito.when(userRepository.findById(1L))
                .thenReturn(Optional.of(userFromDb));
        User userFromService = userService.findById(1L);
        Mockito.verify(userRepository)
                .findById(1L);
        userService.changePassword(userFromService.getId(), "1234");
        Assertions.assertEquals(passwordEncoder.encode("1234"), userFromService.getPassword());
    }
}