package by.maksimovich.rental.entity;

/**
 * @author Maksim Maksimovich
 */
public enum OrderStatus {
    FINISHED, OPEN, CANCELED, DELETED
}
