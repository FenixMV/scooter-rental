package by.maksimovich.rental.entity;

/**
 * @author Maksim Maksimovich
 */
public enum MembershipStatusInRentalPoint {
    AVAILABLE, DELETED, BLOCKED, FINISHED
}
