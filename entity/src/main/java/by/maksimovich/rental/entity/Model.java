package by.maksimovich.rental.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Maksim Maksimovich
 */
@Entity
@Table(name = "models")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Model extends ParentEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "modification")
    private String modification;

    @Column(name = "max_speed")
    private BigDecimal maxSpeed;

    @Column(name = "max_load")
    private BigDecimal maxLoad;

    @Column(name = "weight")
    private BigDecimal weight;

    @Column(name = "model_status")
    @ColumnTransformer(read = "UPPER(model_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private ModelStatus modelStatus;
}
