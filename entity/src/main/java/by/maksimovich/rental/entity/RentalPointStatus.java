package by.maksimovich.rental.entity;

/**
 * @author Maksim Maksimovich
 */
public enum RentalPointStatus {
    ACTIVE, DELETED
}
