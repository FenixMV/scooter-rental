package by.maksimovich.rental.entity;

/**
 * @author Maksim Maksimovich
 */
public enum ModelStatus {
    PRODUCED, NOT_PRODUCED
}
