package by.maksimovich.rental.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author Maksim Maksimovich
 */
@Entity
@Table(name = "rental_points")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RentalPoint extends ParentEntity {

    @Column(name = "location")
    private String location;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "scooters_capacity")
    private int scootersCapacity;

    @Column(name = "rental_point_status")
    @ColumnTransformer(read = "UPPER(rental_point_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private RentalPointStatus rentalPointStatus;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "latitude")
    private double latitude;

    @OneToMany(mappedBy = "rentalPoint", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Set<Scooter> scooters;

    @OneToMany(mappedBy = "rentalPoint", cascade = CascadeType.REFRESH)
    private List<Membership> memberships;

    @ManyToMany
    @JoinTable(name = "rental_point_payment_types",
            joinColumns = {@JoinColumn(name = "rental_point_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "payment_types_id", referencedColumnName = "id")})
    private Set<PaymentType> paymentTypes;

    public RentalPoint(Long id, String location, String name, int scootersCapacity, RentalPointStatus rentalPointStatus,
                       double longitude, double latitude) {
        this.id = id;
        this.location = location;
        this.name = name;
        this.scootersCapacity = scootersCapacity;
        this.rentalPointStatus = rentalPointStatus;
        this.longitude = longitude;
        this.latitude = latitude;
    }
}
