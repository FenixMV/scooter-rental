package by.maksimovich.rental.entity;

/**
 * @author Maksim Maksimovich
 */
public enum UserStatus {
    ACTIVE, NOT_ACTIVE, DELETED
}
