package by.maksimovich.rental.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Maksim Maksimovich
 */
@Entity
@Table(name = "orders")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order extends ParentEntity {

    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @Column(name = "order_time")
    private LocalDateTime orderTime;

    @Column(name = "order_status")
    @ColumnTransformer(read = "UPPER(order_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Column(name = "mileage")
    private BigDecimal mileage;

    @Column(name = "order_finish_time")
    private LocalDateTime orderFinishTime;

    @Column(name = "payment_method")
    private String paymentMethod;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "scooter_id", referencedColumnName = "id")
    private Scooter scooter;

    @ManyToOne
    @JoinColumn(name = "membership_id", referencedColumnName = "id")
    private Membership membership;
}
