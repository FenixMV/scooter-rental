package by.maksimovich.rental.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Maksim Maksimovich
 */
@Entity
@Table(name = "scooters")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Scooter extends ParentEntity {

    @Column(name = "power_reserve")
    private BigDecimal powerReserve;

    @Column(name = "price_per_minute")
    private BigDecimal pricePerMinute;

    @Column(name = "scooter_availability_status")
    @ColumnTransformer(read = "UPPER(scooter_availability_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private ScooterOrderStatus availabilityStatus;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "model_id", referencedColumnName = "id")
    private Model model;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rental_point_id", referencedColumnName = "id")
    private RentalPoint rentalPoint;
}
