package by.maksimovich.rental.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Maksim Maksimovich
 */
@Entity
@Table(name = "memberships")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Membership extends ParentEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "action_time")
    private int actionTime;

    @Column(name = "purchase_time")
    private LocalDateTime purchaseTime;

    @Column(name = "activation_time")
    private LocalDateTime activationTime;

    @Column(name = "expiration_time")
    private LocalDateTime expirationTime;

    @Column(name = "membership_status")
    @ColumnTransformer(read = "UPPER(membership_status)", write = "LOWER(?)")
    @Enumerated(EnumType.STRING)
    private MembershipStatusInRentalPoint membershipStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rental_point_id", referencedColumnName = "id")
    private RentalPoint rentalPoint;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
}
