package by.maksimovich.rental.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * @author Maksim Maksimovich
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "payment_types")
public class PaymentType extends ParentEntity {

    @Column(name = "type")
    private String type;

    @ManyToMany(mappedBy = "paymentTypes")
    private List<RentalPoint> rentalPoints;
}
