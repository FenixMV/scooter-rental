package by.maksimovich.rental.entity;

/**
 * @author Maksim Maksimovich
 */
public enum ScooterOrderStatus {
    AVAILABLE, UNAVAILABLE
}
